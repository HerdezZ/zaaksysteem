# Zaaksysteem.nl

This is the main code repository for the Zaaksysteem framework.

For more information, visit our [website](http://www.zaaksysteem.nl/).

## Short technical introduction

All development for Zaaksysteem follows the following procedure when it comes
to getting the code in this codebase. Since this is open source software, you
are free to fork, modify, redistribute and open pull-requests, under the
limitations of the
[EUPL license](http://joinup.ec.europa.eu/software/page/eupl).

## Branches and tags

* Production releases
    Ever production release is tagged.

* master
    This branch is considered stable and can be used on production
    environments. From this branch tags are made and is the branch used to
    branch of for bugfixes and new features.

* development
    This branch contains active and ongoing development modifications, it is
    *unstable* as it may contain modifications that have not been tested fully.
    This branch is reset every two weeks to the tip of `trial`.

## Contributing

You are free to submit pull-requests for improvements to Zaaksysteem. Please
target those requests to our most recent sprint branch, and describe
liberally what the change does, why it does so, and what you believe the impact
will be.
You can have a look at our default commit message template or use it by
default: `git config commit.template .default-commit-message`

When adding code, please try to make sure your editor understands the
[`.editorconfig`](http://editorconfig.org) found in the root of the repository.

# Running the development environment

Zaaksysteem uses [docker](http://www.docker.com/) to manage development
environments that are the same for every developer. To create a new
environment, you first need to install `docker` and `docker-compose`.
For docker please follow the installation instructions as found on the
[docker documenation page](https://docs.docker.com/engine/installation/).

Docker is very disk consuming, make sure you have sufficient space
somewhere for docker to use. One can tweak the default `/var/lib/docker`
to be elsewhere. On Debian/Ubuntu edit `/etc/default/docker` and add the
`-g` option: `DOCKER_OPTS="-g /path/to/free/space"`.  On Fedora/CentOS
one should edit `/etc/sysconfig/docker`, eg `other_args="-g
/path/to/free/space"`. For more information see the [docker forums]
(https://forums.docker.com/t/how-do-i-change-the-docker-image-installation-directory/1169).

You can reclaim disk space from old (unused) container images using:

`docker image prune`

You can (re)create all the containers by running this:
```
# Only rebuild frontend container
./dev-bin/docker-maintenance.sh frontend

# Only rebuild frontend and backend container
./dev-bin/docker-maintenance.sh frontend backend

# Rebuild all the containers
./dev-bin/docker-maintenance.sh
```

## macOS

For macOS it is advised that you configure docker to to have at least 8Gb
of memory. Otherwise you may end up with build errors.

## Generate configuration files
When you have completed the instalation you need to create some
configuration files by running `dev-bin/generate_config.sh`.

## Start your docker

You can now start your development environment by running:

```
$ docker-compose up
```

This will build and start all the relevant containers.

You can connect to the development environment on https://dev.zaaksysteem.nl/
now.

## SSL certificates

After building the frontend container, you can extract the generated
development CA certificate from it using:

```
$ ./dev-bin/get_ca_certificate.sh > ca.crt
```

By importing that certificate on your system, all connections to
`dev.zaaksysteem.nl` will be trusted (there won't be certificate warnings to
ignore anymore).

## Override the docker-compose.yml file

In case you want to override certain `docker-compose.yml` entries but you
don't want to check them you can make use of the
`docker-compose.overide.yml` file, you can find a working example in
`docker-compose.overide.example`.

After editting this file you need to recreate the containers:
```
docker-compose rm -s -f <container>

docker-compose up --no-start <container>
docker-compose start <container>

# Or just
docker-compose up -d <container>
```

## Front-end development

When starting frontend development you will need the
`docker-compose.override.example` as your source of the your
`docker-compose.override.yml`. It defines the last stage of the build:
`npm-development`. See `docker/Dockerfile.frontend` for more information.

### docker.override.yml
```
frontend:
  build:
    target: "npm-development"
```

### Run frontend run!

```
# Setup frontend container for development
# Done via npm-development build, but MacOS users may want this
# Linux devs may skip this part
./dev-bin/run-frontend.sh init

# Once you have a running container and want to start your day
./dev-bin/run-frontend.sh dev

# Clean up the development container
./dev-bin/run-frontend.sh clean
# or just
docker-compose rm -f -s frontend
docker-compose up frontend
```


## Backend development

Building a new Perl docker layer can be done by running:
`dev-bin/docker-perl`.

The base image will only be build if the checksum of the `cpanfile` and
`docker/Dockerfile.perl` does not match the reference in the
`docker/Dockerfile.backend`.

It is advised to pin versions of external modules in case you are using
git repositories. This is because the docker build otherwise uses the
cache. You could also force a non-cached build.

Pushing the new layer is done like so (this can only be done by Mintlab
developers): `dev-bin/docker-perl -p`

## Commit hooks

Feel free to use the following commit hooks:

```
./dev-bin/tests.hook
```

This hook allows for two environment variables: `ZS_PROVE` and `ZS_NPM`.
The former is to run the perl testsuite and the later is to run the JS
linter + testsuite. When no environment variable is found both suites
will be run. Running the backend testsuite requires a running instance
of redis and the backend container. The frontend testsuite requires a
running frontend container: `docker-compose start redis backend
frontend`.

You can put the hooks in `.git/hooks/pre-commit` or in
`.git/hooks/pre-push` depending on your own preferences.

## End-to-end testing (E2E)

### Test preparations
Before starting with tests, make sure that docker, node and npm are installed.

#### Docker-compose override:
Add/merge the following to the required 'docker-compose.override.yml'-file in the project folder:

```
services:
    frontend:
        build:
            target: "npm-development"
```

#### Docker
Run docker with:

```
docker-compose up
```

#### Webpack
Run the server in /e2e/ with:

```
npm run e2e-server
```
Note: The first time 'npm run update' should be run.

#### Database

Run the following:

```
./dev-bin/testbase.sh --pull
./dev-bin/testbase.sh --load
./dev-bin/testbase.sh --create
```

For more information on the database management see below.

### Run tests

All commands in /e2e/.

#### To run all the tests

```
npm run test
```

#### To run predefined sets

```
npm run test --set=setname
```

The names of the configured sets can be found in the protractor configuration file 'protractor.config.js' as keys of the Object 'sets' or 'paths'.

#### To run the tests of a specific folder and/or file

```
npm run test --folder=foldername --file=filename
```

#### To run the tests in parallel

```
npm run e2e --sessions=3
```

When running more tests than sessions, remaining tests will start when finished tests get closed. For example: Running A, B, C, D with three sessions, A, B and C will start simultaneously. D will start when either A, B, or C finishes.

#### To run the tests for multiple browsers

```
npm run test --browser=browsername
```

By default the tests will be run for chrome only. To run tests in a different browser set browser as 'chrome', 'firefox', 'internet explorer', 'MicrosoftEdge'. To run tests for all browsers set browser as 'all'.

#### To run the tests for a different subdomain

```
npm run test --domain=domainname
```

By default the tests will be run for 'https://testbase-instance.dev.zaaksysteem.nl'.

### Database management

A database has been created on which the testscenarios ought to be run. This database contains
fixtures, such as cases/contacts/etc, that the testscenarios expect to exist. The image of this
database is held in a remote repository. Two databases must be created using this image:
1. 'testbase' for maintaining the fixtures (https://testbase.dev.zaaksysteem.nl)
2. 'testbase-instance' to run the tests on (https://testbase-instance.dev.zaaksysteem.nl)

#### Script
A script has been created to manage loading and dumping that test database,
and to manage the external repository (clone, commit, etc.).

```
./dev-bin/testbase.sh <option>
```

Running the script will show an explanation of the different options.
* pull      - Retrieve latest version of 'testbase' git repository
* load      - (Re)load the template database and files from git repository
* save      - Save template database and files to the git repository
* commit    - Commit changes in the 'testbase' git submodule
* push      - Push changes in the 'testbase' git submodule
* create    - Copy template database and files for a test run
* drop      - Remove test run database and files

#### Create the databases
Simply run 'pull' and 'load' to create/overwrite 'testbase' from the remote image.
Then run 'create' to create/overwrite 'testbase-instance' from 'testbase.

#### Running tests
To run tests on a clean enviroment, always run 'create' before running the tests.
This may be skipped when running view-only tests or during development.

#### Developing new fixtures
New fixtures are developed on 'testbase'.
* Always run 'load' before starting, to ensure that you're working on a clean database
* Test new fixtures on 'testbase-instance' by running 'create' (do not use 'load' now!)
* Run 'save', 'commit' and 'push' to push your new database/fixtures to the remote

Tips:
Creating the fixtures is an all-or-nothing deal. A mistake has to be either accepted,
or testbase should be reset entirely using 'load' to start from scratch. Work out the
desired test scenario's and their requirements in advance (e.g. in excel), and take a
moment to think them all through.

Do not share fixtures between testscenario's. A testscenario should not be dependent
on the execution of another testscenario. It should be possible to run any subset in
any order.

#### Database upgrades
Database upgrades can be done as usual:

```
docker-compose exec database bash
cd /opt/zaaksysteem/db/upgrade/<version>
psql -U zaaksysteem testbase -f <filename>
```

#### Database reference
The zaaksysteem project contains a reference to the last commit on 'testbase'. Therefore,
when any database change is made, the new reference has to also be committed. When developing
new testscenario's it is recommended to commit the new reference together with the new
scenario's.

## Connect with your database

To connect to the database. you can you can run the PostgreSQL client in the
database container:

```
$ docker-compose exec database psql -U zaaksysteem
```

The "db/" directory in the source is available as "/opt/zaaksysteem/db" in the
container.

It's also possible to connect to port 5432 on localhost, using a tool like
PgAdmin. The username is "zaaksysteem" and the password "zaaksysteem123"

## Contained email

Some parts of Zaaksysteem send email. As long as the backend is not configured
to use an external mail server (this is th default), all email messages
are sent to small Mailhog container. You can access this container by going to
http://localhost:8025.

## StatsD

You can configure your Zaaksysteem to send statistics to statsd. You can access
the Graphite webinterface via http://localhost:82.

### Support

We only support the community version of the software through the
[wiki](http://wiki.zaaksysteem.nl/). For professional support, please contact
[Mintlab](http://www.zaaksysteem.nl/).

--
The [Mintlab](http://www.mintlab.nl/) Team
