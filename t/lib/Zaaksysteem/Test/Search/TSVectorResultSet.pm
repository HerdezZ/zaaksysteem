package Zaaksysteem::Test::Search::TSVectorResultSet;

use Zaaksysteem::Test;

use Zaaksysteem::Mock::Backend::Object::Data::ResultSet;

sub test_text_vector_condition {
    my $test = shift;

    my $rs = Zaaksysteem::Mock::Backend::Object::Data::ResultSet->new;

    my $cond = $rs->build_text_vector_condition;

    ok !defined $cond, 'empty keyword argument list';

    is_deeply $rs->build_text_vector_condition('a'), {
        tsvector_column => { '@@' => 'a:*' }
    }, 'single keyword tsquery';

    is_deeply $rs->build_text_vector_condition('a', 'b'), {
        tsvector_column => { '@@' => 'a:* & b:*' }
    }, 'multiple keyword arguments tsquery';

    is_deeply $rs->build_text_vector_condition('a b'), {
        tsvector_column => { '@@' => 'a:* & b:*' }
    }, 'multiple keywords tsquery';

    is_deeply $rs->build_text_vector_condition('a', 'b c'), {
        tsvector_column => { '@@' => 'a:* & b:* & c:*' }
    }, 'multiple keywords arguments tsquery';

    is_deeply $rs->build_text_vector_condition(1, 2, '3'), {
        tsvector_column => { '@@' => '1:* & 2:* & 3:*' }
    }, 'integer keyword value type';

    dies_ok {
        $rs->build_text_vector_condition(\1)
    } 'non-scalar/string input';
}

sub test_text_vector_search {
    my $test = shift;

    my $rs = Zaaksysteem::Mock::Backend::Object::Data::ResultSet->new;

    $rs->search_text_vector('a');

    ok $rs->_was_searched, 'search triggered';

    is_deeply $rs->_search->[0], {
        tsvector_column => { '@@' => 'a:*' }
    }, 'keyword argument passed';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
