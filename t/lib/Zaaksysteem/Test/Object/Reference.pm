package Zaaksysteem::Test::Object::Reference;

use Zaaksysteem::Test;
use Zaaksysteem::Object::Reference;

=head1 NAME

Zaaksysteem::Test::Object::Reference - Object reference(instance) tests

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Object::Reference

=cut

sub test_ref_instance {
    my $id = '0b3de1c7-0746-4ebc-8b54-f1c85df5c7f3';

    subtest 'object_reference_instance_constructor', sub {
        my $ref = Zaaksysteem::Object::Reference::Instance->new(
            id => $id,
            type => 'some_random_type'
        );

        isa_ok $ref, 'Zaaksysteem::Object::Reference::Instance',
            'instance constructor';

        is $ref->id, '0b3de1c7-0746-4ebc-8b54-f1c85df5c7f3',
            'instance constructor sanity check on id';

        is $ref->type, 'some_random_type',
            'instance constructor sanity check on type';

        ok $ref->TO_STRING =~ m[^some_random_type\(\.\.\.(?<id>\w+)\)$],
            'instance stringification matches expected style';

        ok $id =~ m[\Q$+{id}\E$], 'instance stringification has id part';

        ok !defined $ref->_stringification, '_stringification empty';
        ok !$ref->_instantiable, 'reference not instantiable';

        my $ex = exception { $ref->_instance };

        isa_ok $ex, 'BTTW::Exception::Base', 'proper exception object thrown';
        is $ex->type, 'object/ref/instantiator/missing',
            'thrown exception on missing instantiator';
    };

    subtest 'object_reference_instance_preview', sub {
        my $ref = Zaaksysteem::Object::Reference::Instance->new(
            id => $id,
            type => 'some_random_type',
            preview => 'abc'
        );

        is $ref->TO_STRING, 'abc', 'preview string used';
        ok defined $ref->_stringification, '_stringification filled';
    };

    subtest 'object_reference_instance__stringification', sub {
        my $ref = Zaaksysteem::Object::Reference::Instance->new(
            id => $id,
            type => 'some_random_type',
            _stringification => 'abc'
        );

        is $ref->TO_STRING, 'abc', '_stringification attribute';

        is "$ref", 'abc', 'object overloads stringification';
    };

    subtest 'object_reference_instance_instantiator', sub {
        my $instance = 'invalid';

        my $ref = Zaaksysteem::Object::Reference::Instance->new(
            id => $id,
            type => 'foo',
            instantiator => sub { return $instance }
        );

        ok $ref->_instantiable, 'reference claims intantiability';

        dies_ok {
            $ref->_instance
        } 'reference with invalid instantiator throws exception';

        $instance = TestObject->new(id => $id, foo => 'bar');

        lives_ok {
            is $ref->_instance->id, $id,
                'returned instance has expected id';
            is $ref->_instance->foo, 'bar',
                'returned instance has expected attribute value';
        } 'reference instance instantiator lives';

        my $new_ref = $instance->_ref;

        is $new_ref->id, $instance->id,
            'fresh reference has expected id';

        is $new_ref->type, $instance->type,
            'fresh reference has expected type';

        is $new_ref->TO_STRING, 'bar',
            'fresh reference has expected stringification';
    };
}

package TestObject;

use Moose;

extends 'Zaaksysteem::Object';

has foo => ( is => 'rw' );

override TO_STRING => sub { return shift->foo };

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
