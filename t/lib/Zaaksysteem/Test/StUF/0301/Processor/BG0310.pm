package Zaaksysteem::Test::StUF::0301::Processor::BG0310;

use Zaaksysteem::Test::Moose;
use Zaaksysteem::Test;
use Zaaksysteem::StUF::0301::Processor::BG0310;
use IO::All;
use BTTW::Tools::RandomData qw(generate_uuid_v4);

sub _get_model {
    my $processor = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema            => mock_one,
        reference_id      => int(rand(100000)),
        municipality_code => int(rand(9999)),
        interface_uuid    => generate_uuid_v4,
        @_,
    );
    isa_ok($processor, "Zaaksysteem::StUF::0301::Processor::BG0310");
    return $processor;
}

sub test_marriages {

    my $model = _get_model();
    my $xml = io->catfile(qw(t data StUF 3.10 prs npsLa01-huwelijken.xml))->slurp;

    my $found = $model->parse_multiple_np($xml);
    is(@$found, 1, "Found one person");
    cmp_deeply(
        $found->[0]{subject}{partner},
        { family_name => "Partnernaampje" },
        "Found partnernaam in bg:geslachtsnaamPartner"
    );

    $xml = io->catfile(qw(t data StUF 3.10 prs npsLa01-huwelijken-other.xml))->slurp;

    $found = $model->parse_multiple_np($xml);
    is(@$found, 1, "Found one person");

    cmp_deeply(
        $found->[0]{subject}{partner},
        {
            family_name       => "Oude naam",
            personal_number   => 922753957,
            personal_number_a => 922753957,
            prefix            => undef,
        },
        "Found partnernaam in huwelijks segment",
    );
}

1;

__END__

=head1 NAME

Zaaksysteem::Test::StUF::0301::Processor::BG0310 - A BG0310 tester

=head1 DESCRIPTION

Test processing stuf0310 messages

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::StUF::0301::Processor::BG0310

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
