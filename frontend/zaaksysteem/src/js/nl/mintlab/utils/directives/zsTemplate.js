/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.directives')
		.directive('zsTemplate', [ '$parse', '$compile', function ( $parse, $compile ) {
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var holder = angular.element('<div></div>');
						
						function parseTemplate ( ) {
							var templateString = attrs.zsTemplate,
								childNodes;
							holder[0].innerHTML = templateString;
							childNodes = holder[0].childNodes;
							
							element[0].innerHTML = '';
							
							element.append($compile(childNodes)(scope));
						}
						
						attrs.$observe('zsTemplate', function onTemplateChange ( ) {
							parseTemplate();
						});
						
					};
				}
				
			};
			
		}]);
	
})();
