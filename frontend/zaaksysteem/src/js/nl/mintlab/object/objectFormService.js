/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.object')
		.factory('objectFormService', [ '$q', '$http', 'formService', function ( $q, $http, formService ) {
			
			var objectFormService = {},
				promisesById = {};
			
			objectFormService.getForm = function ( objectId ) {
				var deferred = promisesById[objectId];
				
				if(deferred) {
					return deferred.promise;
				}
				
				deferred = $q.defer();
				promisesById[objectId] = deferred;
				
				$http({
					method: 'GET',
					url: '/api/object/' + objectId + '/action/instance_form'
				})
					.success(function ( response ) {
						var form = response.result[0];
						
						_.each(form.fieldsets, function ( fieldset ) {
							_.each(fieldset.fields, function ( field ) {
								var config = formService.getFormFieldForAttributeType(field.type);
								
								angular.extend(field, config);
								
							});
						});
						
						deferred.resolve(response.result[0]);
					})
					.error(function ( response ) {
						deferred.reject(response.result ? response.result[0] : response);
					});
					
				return deferred.promise;
			};
			
			return objectFormService;
			
		}]);
	
})();
