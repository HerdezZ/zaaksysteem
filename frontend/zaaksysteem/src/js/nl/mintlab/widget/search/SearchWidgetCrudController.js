/*global angular,_,ezra_dialog,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
	.controller('nl.mintlab.widget.search.SearchWidgetCrudController', [ '$window', '$scope', '$parse', '$interpolate', 'zqlEscapeFilter', 'translationService', 'safeApply', function ( $window, $scope, $parse, $interpolate, zqlEscapeFilter, translationService, safeApply ) {

			var buildUrl = window.zsFetch('nl.mintlab.utils.buildUrl'),
				cancelEvent = window.zsFetch('nl.mintlab.utils.events.cancelEvent');
			
			function getObjectTypeName ( ) {
				var objType = $scope.getObjectType(),
					name;
					
				if(objType) {
					name = objType.object_type;
				}
					
				return name;
			}
			
			function getItemIdKey ( ) {
				var key;
				switch(getObjectTypeName()) {
					default:
					key = 'id';
					break;
					
					case 'case':
					key = 'values[\'case.number\']';
					break;
				}
				return key;
			}
			
			function getItemIdName ( ) {
				var name;
				switch(getObjectTypeName()) {
					default:
					name = 'object.uuid';
					break;
					
					case 'case':
					name = 'case.number';
					break;
				}
				return name;
			}
			
			function getItemId ( item ) {
				return $parse(getItemIdKey())(item);
			}
			
			$scope.getSelectionZql = function ( params ) {
				var selection = $scope.zsCrud.getSelection(),
					selectionType = $scope.zsCrud.getSelectionType(),
					objType = $scope.getObjectType(),
					zqlWhere = $scope.getZqlWhere(),
					zqlMatching = $scope.getZqlMatching(),
					where = '',
					matching = '',
					ids,
					base = '/api/object/search/',
					url;
					
				if(selectionType === 'subset') {
					where = getItemIdName() + ' IN ';
					ids = _.map(selection, function ( item ) {
						return zqlEscapeFilter(getItemId(item));
					});
					where += '(' + ids.join(',') + ')';
				} else {
					where = zqlWhere;
					matching = zqlMatching;
				}

				url = buildUrl(base, {
					zapi_no_pager: 1,
					zql: 'SELECT ' + $scope.getZqlColumns(_.pick(params, 'without', 'visibleOnly')) + ' FROM ' + objType.object_type + (matching ? ' MATCHING ' + matching : '') + (where ? (' WHERE ' + where) : '')
				});

				return url;
			};
			
			$scope.getEzraDialogSelectionParameters = function ( /*action*/ ) {
				var selection = $scope.zsCrud.getSelection(),
					selectionIds,
					selectionType;
					
				selectionIds = _.map(selection, getItemId);
				
				if($scope.zsCrud.getSelectionType() === 'all') {
					selectionType = 'zql';
				} else {
					selectionType = 'selected_cases';
				}
				
				return {
					'selected_case_ids': selectionIds.join(','),
					// TODO: move this somewhere else
					'selection': selectionType,
					'zql': $scope.getZql(),
					'no_redirect': 1
				};
			};
			
			$scope.getExportZql = function ( ) {
				
				var zql = $scope.getSelectionZql({
						without: [ 'case.num_unaccepted_updates', 'case.num_unaccepted_files' ],
						visibleOnly: true
					});
				
				return zql;
			};

			function getActionClickHandler ( action ) {
				var func;
				switch(action.type) {
					case 'url':
					func = function ( ) {
						$window.location.href = $interpolate(action.data.url)({ selectedItems: $scope.zsCrud.getSelection() });
					};
					break;

					case 'ezra-dialog':
					func = function ( event ) {

						var params = _.assign({}, action.data, { cgi_params: $scope.getEzraDialogSelectionParameters(action) });

						function onDialogClose ( ) {
							
							$.ztWaitStop();

							$('#dialog').off('dialogclose', onDialogClose);

							safeApply($scope, function ( ) {
								$scope.$emit('zs.ezra.dialog.close');
							});
						}

						$('#dialog').on('dialogclose', onDialogClose);
						
						ezra_dialog(params, angular.noop);

						cancelEvent(event);
					};
					break;
				}

				return func;
			}
			
			$scope.$on('zs.search.bulk.attempt', function ( ) {
				$scope.$parent.loading.bulk = true;
			});
			
			$scope.$on('zs.search.bulk.success', function ( event, messages ) {
                setTimeout(function () {
                    $scope.$parent.loading.bulk = false;
                    $scope.zsCrud.reloadData(); 
                }, 1500)
				
				if(!messages.length) {
					messages.push({
						message: 'Actie succesvol uitgevoerd op de geselecteerde zaken'
					});
				}
				
				_.each(messages, function ( msg ) {
					$scope.$emit('systemMessage', {
						type: 'info',
						content: msg.message
					});
				});
			});
			
			$scope.$on('zs.search.bulk.error', function ( event, messages) {
				$scope.$parent.loading.bulk = false;
				$scope.$emit('systemMessage', {
					type: 'error',
					content: messages ? messages[0] : translationService.get('Er ging iets fout bij het uitvoeren van de acties. Probeer het opnieuw')
				});
			});
			
			$scope.$watch('loading', function ( ) {
				$scope.setCrudLoading($scope.loading);
			});
			
			$scope.$on('zs.crud.item.change', function ( event, items ) {

				items.forEach(function ( item ) {
					_.assign(item,
						{
							itemActions:
								$scope.getItemActions(items)
									.map(function ( action ) {
										return _.assign(
											{},
											action,
											{
												when: true,
												name: action.id,
												click: getActionClickHandler(action)
											}
										);
									})
						}
					);
				});

			});
			
			$scope.$on('search.reload', function ( ) {
				if($scope.zsCrud) {
					$scope.zsCrud.reloadData();
				}
			});
			
		}]);
	
})();
