/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsSpotEnlighterFormField', [ '$parse', function ( $parse ) {
			
			return {
				require: 'ngModel',
				scope: true,
				controller: [ '$scope', function ( $scope ) {
					
					var ctrl = this;
					
					ctrl.getParams = function ( ) {
						var params = $scope.field.data.params;
						
						if(ctrl.isInactiveToggleVisible()) {
							params = angular.copy(params) || {};
							params.inactive = !!ctrl.includeInactive;
							if(params.active === undefined) {
								params.active = true;
							}
						}
						
						return params;
						
					};
					
					ctrl.isInactiveToggleVisible = function ( ) {
						return $scope.getRestrict($scope.field) === 'contact/medewerker';
					};
					
					ctrl.transform = function ( $object ) {
						var obj;
						
						if($scope.field.data.transform) {
							obj = $parse($scope.field.data.transform)($scope, { $object: $object });
						} else {
							obj = $object;
						}
						
						return obj;
					};
					
					return ctrl;
					
				}],
				controllerAs: 'zsSpotEnlighterFormField', 
				link: function ( scope, element, attrs, ngModel ) {
					
					function isMultiple ( ) {
						var multiple = attrs.zsSpotEnlighterFormFieldMultiple;
						if(multiple === 'false' || multiple === '0') {
							multiple = false;
						}
						return !!multiple;
					}
					
					// function getLimit ( ) {
						
					// }
					
					function transformObject ( obj ) {
						if(attrs.zsSpotEnlighterFormFieldTransform && obj) {
							obj = $parse(attrs.zsSpotEnlighterFormFieldTransform)(scope, {
								'$object': obj
							});
						}
						
						return obj;
					}
					
					function getObjId ( obj ) {
						var resolve = attrs.zsSpotEnlighterResolve || 'id';
						return $parse(resolve)(obj);
					}
					
					function getObjById ( objId ) {
						return _.find(ngModel.$viewValue, function ( obj ) {
							var id = getObjId(obj);
							return objId === id;
						});
					}
					
					scope.add = function ( obj ) {
						var val = ngModel.$viewValue ? ngModel.$viewValue.concat() : [];
						
						if(!scope.has(obj)) {
							val.push(obj);
							ngModel.$setViewValue(val);
						}
					};
					
					scope.remove = function ( obj ) {
						var val = ngModel.$viewValue ? ngModel.$viewValue.concat() : [],
							index;
							
						obj = getObjById(getObjId(obj));
						
						if(scope.has(obj)) {
							index = _.indexOf(val, obj);
							if(index !== -1) {
								val.splice(index, 1);
								ngModel.$setViewValue(val);
							}
						}
						
					};
					
					scope.has = function ( obj ) {
						return !!getObjById(getObjId(obj));
					};
					
					scope.getObjLabel = function ( obj ) {
						var label = $parse(attrs.zsSpotEnlighterLabel)(obj);
						return label;
					};
					
					scope.getList = function ( ) {
						return ngModel.$viewValue || [];
					};
					
					ngModel.$isEmpty = function ( value ) {
						var multi = isMultiple(),
							empty;
							
						if(multi) {
							empty = !value || !value.length;
						} else {
							empty = !value;
						}
						
						return empty;
					};
					
					scope.$watch('obj', function ( ) {
						var object = transformObject(scope.obj);
 						
						if(isMultiple() && scope.obj) {
 							if(object) {
 								scope.add(object);
 							}
 							scope.obj = null;
						} else if(!isMultiple()) {
							ngModel.$setViewValue(object);
						}
					});
					
					ngModel.$formatters.push(function ( val ) {
						var arr;
						if(isMultiple()) {
							
							arr = angular.copy(val);
							
							if(!angular.equals(ngModel.$modelValue, arr)) {
								ngModel.$setViewValue(arr);
							}
							
						} else {
							scope.obj = val;
						}
						return val;
					});
					
				}
			};
			
		}]);
	
})();
