/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudActionDropDownController', [ '$scope', function ( $scope ) {
			
			$scope.$on('zs.ezra.dialog.open', function ( ) {
				$scope.closePopupMenu();
			});
			
			$scope.$on('zs.crud.action.click', function ( ) {
				$scope.closePopupMenu();
			});
			
		}]);

})();