const { join, resolve } = require('path');
const { DefinePlugin, DllReferencePlugin } = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CustomServiceWorkerPlugin = require('./CustomServiceWorkerPlugin');
const autoprefixer = require('autoprefixer');

const PUBLIC_PATH = '/assets/';
const ROOT = join(__dirname, '..', 'root');
const SUPPORT_PROXY_URL = 'https://localhost:1050';
const isDev = process.env.NODE_ENV === 'development';
const exclude = /node_modules/;

const appNames = ['intern', 'mor', 'vergadering', 'wordaddin'];
const clientSrc = join(__dirname, 'src');

const extractStyle = use => {
  return isDev
    ? use
    : ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use
      });
};

const apps = {
  paths: Object.fromEntries(
    appNames.map(app => [app, join(clientSrc, app, 'index.js')])
  ),
  htmlOptions: appNames.map(app => ({
    base: `/${app}/`,
    chunks: [app],
    filename: join(app, 'index.html'),
    inject: 'body',
    template: join(clientSrc, app, 'index.ejs'),
    title: 'Zaaksysteem'
  })),
  serviceWorkerOptions: appNames.map(app => ({
    root: ROOT,
    publicPath: PUBLIC_PATH,
    entries: [app],
    globs: [join(app, 'index.html')],
    options: {
      navigateFallback: `${PUBLIC_PATH}${app}/index.html`,
      navigateFallbackWhitelist: [new RegExp(`^/${app}`)]
    },
    external: ['vendors.bundle.js'],
    commons: []
  }))
};

module.exports = {
  devtool: 'source-map',
  entry: { ...apps.paths },
  output: {
    filename: '[name].js',
    path: join(ROOT, PUBLIC_PATH),
    publicPath: PUBLIC_PATH
  },
  resolve: {
    alias: {
      openlayers: resolve(__dirname, 'openlayers')
    }
  },
  module: {
    rules: [
      {
        test: /.*\.js$/,
        loader: 'babel-loader',
        exclude: [exclude, /frontend/, /openlayers/],
        options: {
          presets: ['babel-preset-es2015'].map(require.resolve),
          plugins: ['babel-plugin-add-module-exports'].map(require.resolve)
        }
      },
      {
        test: /.*\.html$/,
        // removing optional tags breaks HtmlWebpackPlugin script injection
        use:
          'html-loader?removeOptionalTags=false&removeDefaultAttributes=false'
      },
      {
        test: /\.(scss|css)$/,
        use: extractStyle([
          {
            loader: 'css-loader',
            options: {
              minimize: false,
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [autoprefixer],
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: { sourceMap: true }
          }
        ])
      },
      {
        test: /\.(woff(2)?|eot|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?name=[name].[ext]&limit=10000'
      },
      {
        test: /\.swig$/,
        loader: resolve(__dirname, 'swig.loader.js'),
        exclude
      }
    ],
    noParse: [
      /[/\\]node_modules[/\\](angular-mocks|angular-ui-router)/,
      /[/\\]node_modules[/\\]api-check/,
      /[/\\]node_modules[/\\]define/,
      /[/\\]node_modules[/\\]email-regex/,
      /[/\\]node_modules[/\\]fuzzy/,
      /[/\\]node_modules[/\\]iban/,
      /[/\\]node_modules[/\\]is-promise/,
      /[/\\]node_modules[/\\]match-media/,
      /[/\\]node_modules[/\\]openlayers/,
      /[/\\]node_modules[/\\]soma-events/,
      /[/\\]node_modules[/\\]word-ngrams/
    ]
  },
  plugins: [
    new CaseSensitivePathsPlugin(),
    new DllReferencePlugin({
      context: process.cwd(),
      manifest: require('./vendors.json')
    }),
    new DefinePlugin({
      ENV: {
        USE_SERVICE_WORKERS: !isDev,
        IS_DEV: isDev,
        DISABLE_ANIMATION: false,
        SUPPORT_PROXY_URL: JSON.stringify(SUPPORT_PROXY_URL)
      }
    }),
    ...apps.htmlOptions.map(options => new HtmlWebpackPlugin(options)),
    ...(isDev
      ? []
      : apps.serviceWorkerOptions.map(
          opt => new CustomServiceWorkerPlugin(opt)
        )),
    isDev
      ? null
      : new OptimizeCssAssetsPlugin({
          cssProcessorOptions: {
            safe: true
          }
        }),
    isDev
      ? null
      : new UglifyJsPlugin({
          uglifyOptions: { mangle: false },
          sourceMap: true
        }),
    isDev
      ? null
      : new ExtractTextPlugin({
          filename: '[name].css',
          allChunks: true
        })
  ].filter(Boolean),
  stats: {
    assets: true,
    cached: false,
    cachedAssets: false,
    children: false,
    chunks: false,
    chunkModules: false,
    chunkOrigins: false,
    colors: true,
    depth: false,
    entrypoints: false,
    errors: true,
    errorDetails: true,
    hash: false,
    maxModules: 0,
    modules: false,
    moduleTrace: false,
    performance: true,
    providedExports: false,
    publicPath: false,
    reasons: false,
    source: false,
    timings: true,
    usedExports: false,
    version: true,
    warnings: true
  }
};
