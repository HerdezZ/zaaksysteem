import composedReducer from './composeReducer';

const scopedReducerFactory = scope => ( ...rest ) =>
	composedReducer({
		scope
	}, ...rest);

export default scopedReducerFactory;
