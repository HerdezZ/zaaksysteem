import angular from 'angular';
import snackbarServiceModule from './../../../ui/zsSnackbar/snackbarService';
import resourceModule from './../../../api/resource';
import composedReducerModule from './../../../api/resource/composedReducer';
import zsTruncateModule from './../../../ui/zsTruncate';
import zsConfirmModule from '../../../ui/zsConfirm';
import template from './template.html';
import zsNotificationListItem from './zsNotificationListItem';
import './styles.scss';

function generateContactDossierLink(notification) {
	const betrokkeneId = notification.logging_id.subject_id;
	const elems = betrokkeneId.split('-');
	return `/betrokkene/${elems[2]}?type=${elems[1]}&gm=1`;
}

export default
	angular.module('zsNotificationList', [
		snackbarServiceModule,
		resourceModule,
		composedReducerModule,
		zsTruncateModule,
		zsConfirmModule,
		zsNotificationListItem
	])
		.directive('zsNotificationList', [
			'$http', '$q', '$window', '$document', '$animate', '$state', 'snackbarService', 'resource', 'composedReducer', 'dateFilter', 'zsConfirm',
			( $http, $q, $window, $document, $animate, $state, snackbarService, resource, composedReducer, dateFilter, zsConfirm ) => {


			return {
				restrict: 'E',
				template,
				scope: {
					onReadMore: '&',
					notifications: '&',
					isLoading: '&',
					onNavigate: '&',
					onArchiveNotification: '&',
					onArchiveAllNotifications: '&',
					onCloseNotifications: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						notificationReducer;

					notificationReducer = composedReducer({ scope }, ctrl.notifications)
						.reduce( notifications => {

							return notifications.map(
								notification => {

									let id = notification.id,
										isRead = notification.is_read === 1,
										type = notification.logging_id.event_type,
										title = notification.logging_id.description,
										caseId = notification.logging_id.case_id,
										icon,
										description,
										message,
										linkObj,
										linkLabel,
										link,
										state;

									switch (type) {
										case 'user/apply':
										icon = 'account-star-variant';
										link = '/medewerker/';
										linkLabel = 'Gebruikerspagina';
										break;

										case 'case/document/create':
										icon = 'file';
										link = `/intern/zaak/${caseId}/documenten/`;
										state = 'case.docs';
										break;

										case 'case/document/accept':
										case 'case/document/assign':
										icon = 'file-check';
										link = `/intern/zaak/${caseId}/documenten/`;
										state = 'case.docs';
										break;

										case 'case/update/field':
										case 'case/pip/updatefield':
										icon = 'pencil';
										link = `/intern/zaak/${caseId}/timeline/`;
										state = 'case.timeline';
										break;

										case 'case/pip/feedback':
										icon = 'email';
										message = {
											sender: notification.aanvrager,
											content: notification.logging_id.content
										};
										link = `/intern/zaak/${caseId}/communicatie/`;
										state = 'case.communication';
										break;

										case 'case/note/create':
										icon = 'message-text';
										state = 'case.timeline';
										link = `/intern/zaak/${caseId}/timeline/`;
										break;

										case 'email/send':
										icon = 'email';
										link = `/intern/zaak/${caseId}/timeline/`;
										state = 'case.timeline';
										break;

										case 'subject/contactmoment/create':
										case 'subject/note/create':
										icon = 'message-text';
										linkLabel = 'Contactdossier';
										link = generateContactDossierLink(notification);
										break;

										default:
										icon = 'message-text';
										state = 'case.phase';
										link = '/intern/zaak/${caseId}/';
										break;

									}

									linkObj = {
										label: linkLabel ? linkLabel : `Zaak ${caseId}`,
										url: caseId !== null ? `/intern/zaak/${caseId}/` : link,
										section: link
									};

									return {
										id,
										caseId,
										title,
										description,
										type,
										icon,
										state,
										link: linkObj,
										message,
										classes: {
											read: isRead
										},
										timestamp: dateFilter(notification.logging_id.timestamp, 'dd-MM-yyyy, HH:mm')
									};
								}
							);

						});

					ctrl.getNotifications = notificationReducer.data;

					ctrl.navigateToNotificationLink = ( obj ) => {
						// When we're in new ZS we can use $state.go for a nice state transition
						if ($state.current.name !== '') {
							if (obj.state) {
								$state.go(obj.state, { caseId: obj.caseId });

							} else {
								$window.location.href = obj.link.url;
							}

						// When we're in old ZS we can't use $state.go so we force navigation
						} else {
							$window.location.href = obj.caseId ? obj.link.section : obj.link.url;
						}

						ctrl.onNavigate();

					};

					ctrl.archiveNotification = ( id ) => {
						ctrl.onArchiveNotification({ $id: id });
					};

					ctrl.archiveAllNotifications = ( ) => {
						zsConfirm('Weet u zeker dat u alle notificaties wilt archiveren?', 'Alles archiveren')
							.then( ( ) => {
								ctrl.onArchiveAllNotifications();
							});
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
