import angular from 'angular';
import includes from 'lodash/includes';
import keyBy from 'lodash/keyBy';
import mapValues from 'lodash/mapValues';
import each from 'lodash/each';
import assign from 'lodash/assign';
import vormTemplateServiceModule from './../../vormTemplateService';
import zsDatePickerModule from '../../../ui/zsDatePicker';
import zsIconModule from './../../../ui/zsIcon';

export default angular
	.module('vorm.types.input', [
		vormTemplateServiceModule,
		zsIconModule,
		zsDatePickerModule
	])
	.filter('absUrl', [() => {
		return ( val ) => {
			return val && !val.match(/^(\w+):\/\//) ?
				`http://${val}`
				: val;
		};
	}])
	.run(['$document', 'vormTemplateService', ( $document, vormTemplateService ) => {
		let templates =
			mapValues(
				keyBy(
					'date datetime datetime-local email month number password search tel text time url week checkbox'
						.split(' ')
				),
				( value, key ) => {

					let placeholder = includes('text search tel url email number password'.split(' '), key) ?
							'placeholder="{{vm.invokeData(\'placeholder\')}}"'
							: '',
						tpl =
							`<input 
								id="{{vm.getInputId()}}"
								name="{{vm.invokeData(\'name\')}}"
								type="${key}" 
								${placeholder} 
								ng-disabled="vm.invokeData(\'fieldDisabled\')" 
								ng-model  
							/>`;

					if (key === 'checkbox') {
						tpl =
							`<label for="{{vm.getInputId()}}">
								${tpl}
								{{vm.invokeData("checkboxLabel")}}
							</label>`;
					}

					return tpl;
				}
			);

		each(templates, ( tpl, type ) => {
			let options = {
				control: angular.element(tpl)
			};

			switch (type) {
			case 'url':
				options.display =
					angular.element(
						`<a 
							class="delegate-value-display" 
							ng-href="{{delegate.value | absUrl}}" 
							target="_blank" rel="noopener" 
							ng-show="delegate.value && delegate.isValid"  
						>{{delegate.value}}<zs-icon icon-type="open-in-new"></zs-icon></a>`
					);

				options.control =
					angular.element(
						`<div>
							${options.control[0].outerHTML}
							<a 
								href="{{delegate.value | absUrl}}" 
								ng-show="delegate.value&&delegate.isValid" 
								target="_blank" rel="noopener"  
							><zs-icon icon-type="open-in-new"></zs-icon></a>
						</div>`
					);
				break;

			case 'date': {
				const input = $document[0].createElement('input');

				input.setAttribute('type', 'date');

				const supportsDate = (input.type === 'date');

				options.control.attr('min', '{{vm.invokeData(\'min\') | date:\'yyyy-MM-dd\' }}');
				// NB: 9999-12-31 is the Pikaday limit
				options.control.attr('max', '{{ (vm.invokeData(\'max\') | date:\'yyyy-MM-dd\') || \'9999-12-31\' }}');

				if (!supportsDate) {
					options.control.attr('zs-date-picker', '');
					options.defaults = assign(
						{},
						options.defaults,
						{
							modelOptions: {
								allowInvalid: false
							}
						}
					);
				}

				options.display = angular.element(
					'<span class="delegate-value-display">{{delegate.value | date:\'dd-MM-yyyy\'}}</span>'
				);
			}
				break;
			}

			vormTemplateService.registerType(type, options);
		});
	}])
	.name;
