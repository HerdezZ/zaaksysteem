import find from 'lodash/find';
import includes from 'lodash/includes';
import toArray from 'lodash/toArray';
import uniq from 'lodash/uniq';

const unsupportedAttributeTypes = [
  'appointment',
  'calendar',
  'calendar_supersaas',
  'bag_adres',
  'bag_adressen',
  'bag_straat_adres',
  'bag_straat_adressen',
  'bag_openbareruimte',
  'bag_openbareruimtes',
  'file',
  'geolatlon',
  'googlemaps',
  'subject'
];

// we only submit attributes with a value, therefore we only check those
// 'fieldDefinitions' contains the types, and vals only contains attributes with values
// so the intersect lets us know whether the form has unsupported attributes with a value
const getAttributeTypeEligibility = (fieldDefinitions, vals) => {
  // - system attributes are prefixed with a '$'
  // - when registering a case from the documentintake and changing the field 'direction' on the form
  //   a seemingly useless value 'undefined' will be added, containing the filename and uuid
  const namesWithValue = Object.keys(vals).filter(val => val[0] !== '$' && val !== 'undefined');
  const definitions = toArray(fieldDefinitions);

  const definitionsWithValue =
		namesWithValue
			.map(name => find(definitions, definition => name === definition.name))
			.filter(Boolean); // Filter out computed values which have no definition

  const definitionsWithValueTypes =
    uniq(definitionsWithValue.map(attr => attr.type));

  const hasUnsupportedAttributeTypesWithValues =
    definitionsWithValueTypes.every(type => !includes(unsupportedAttributeTypes, type));

  return hasUnsupportedAttributeTypesWithValues;
};

export default getAttributeTypeEligibility;
