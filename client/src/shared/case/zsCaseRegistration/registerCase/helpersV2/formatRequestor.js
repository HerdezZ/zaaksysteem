import get from 'lodash/get';

// v1 to v2
const subjectTypeDict = {
  person: 'person',
  company: 'organization',
  employee: 'employee'
};

const formatRequestor = requestor => {
  const subjectType = get(requestor, 'instance.subject_type');

  return {
    type: subjectTypeDict[subjectType],
    id: get(requestor, 'reference')
  };
};

export default formatRequestor;
