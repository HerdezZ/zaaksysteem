import angular from 'angular';
import zsClickOutsideModule from './../../zsClickOutside';
import template from './template.html';
import assign from 'lodash/assign';
import './styles.scss';

export default
	angular.module('zsContextualActionForm', [
		zsClickOutsideModule
	])
		.directive('zsContextualActionForm', [ '$animate', '$compile', '$document', ( $animate, $compile, $document ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					action: '&',
					params: '&',
					onClose: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						el,
						tpl;

					let onKeyUp = ( event ) => {

						if (event.keyCode === 27) {
							scope.$evalAsync(ctrl.onClose({ $event: event }));
						}

					};

					scope.close = ( promise ) => {

						if (promise) {

							$animate.addClass(element, 'hidden');
 
							promise.then(( ) => {
								ctrl.onClose();
							})
								.catch( ( ) => {
									$animate.removeClass(element, 'hidden');
								});
						} else {
							ctrl.onClose();
						}

					};

					scope.params = ctrl.params();

					scope.viewController = ctrl.action().controller;

					assign(scope, ctrl.action().params);

					tpl = angular.element(ctrl.action().template).clone();

					tpl.attr('params', 'params');

					$compile(tpl)(scope, ( e ) => {

						el = e;

						element.find('zs-transclude').replaceWith(el);

					});

					$document.bind('keyup', onKeyUp);

					scope.$on('$destroy', ( ) => {
						$document.unbind('keyup', onKeyUp);
					});

					scope.$on('$stateChangeSuccess', ( event ) => {
						scope.$evalAsync(ctrl.onClose({ $event: event }));
					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
