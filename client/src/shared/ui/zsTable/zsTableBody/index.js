import angular from 'angular';
import template from './template.html';
import cellTemplate from './cell-template.html';
import each from 'lodash/each';
import map from 'lodash/map';
import get from 'lodash/get';

export default
	angular.module('zsTableBody', [
	])
		.directive('zsTableBody', [ '$compile', ( $compile ) => {
			
			return {
				scope: {
					columns: '&',
					rows: '&',
					onRowClick: '&',
					useHref: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					let ctrl = this,
						childScope;

					let recompile = ( ) => {

						let el,
							anchorEl;

						if (childScope) {
							childScope.$destroy();
						}

						childScope = $scope.$new();

						$element.empty();

						el = angular.element(template);

						if (ctrl.useHref()) {
							anchorEl = angular.element('<a ng-href="{{::item.href}}" class="no-underlines"></a>');

							for (let i = 0, attrs = el[0].attributes, l = attrs.length; i < l; ++i) {
								let attr = attrs[i],
									val = attr.value;

								if (attr.name !== 'ng-click') {

									if (attr.name === 'class') {
										val = val.split(' ').concat(anchorEl.attr('class').split(' ')).join(' ');
									}
									anchorEl.attr(attr.name, val);
								}

							}

							el = anchorEl;
						}
						
						each(ctrl.columns(), ( column ) => {

							let cell = angular.element(cellTemplate);
							
							cell.attr('column-id', column.id);
							// currently doesn't support text nodes, needs a wrapper
							cell.append(angular.element(column.template));
							el.append(cell);
						});

						$compile(el)(childScope);

						$element.append(el);

					};

					ctrl.getAriaLabel = item => {
						const columns = ctrl.columns()
							.filter(column => !!column.valuePath)
							.filter(column => {
								return get(item, column.valuePath, null) !== null;
							});

						if (columns.length > 0) {
							return columns
								.map(column => `${column.label}: ${get(item, column.valuePath)}.`)
								.join(' ');
						}

						return '';
					};

					$scope.$watchCollection(
						( ) => map(ctrl.columns(), 'templates'),
						recompile
					);

				}],
				controllerAs: 'zsTableBody'
			};

		}])
		.name;
