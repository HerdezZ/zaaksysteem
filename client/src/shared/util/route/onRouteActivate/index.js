import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import getActiveStates from './../getActiveStates';

export default
	angular.module('onRouteActivate', [
		angularUiRouter
	])
		.decorator('$state', [ '$delegate', '$injector', ( $state, $injector ) => {

			let transitionTo = $state.transitionTo;

			$state.transitionTo = ( ...rest ) => {

				return transitionTo(...rest)
					.then( ( transition ) => {

						let parents = getActiveStates($state.$current);

						parents.forEach(( p ) => {

							if (p.self.onActivate) {
								$injector.invoke(p.self.onActivate, p.self, p.locals.globals);
							}

						});

						return transition;
					});

			};

			return $state;

		}])
		.name;
