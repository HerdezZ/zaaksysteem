import angular from 'angular';
import template from './template.html';

import './styles.scss';

export default
		angular.module('Zaaksysteem.mor.caseListItemListItem', [
		])
		.directive('caseListItemListItem', [ ( ) => {
			return {
				restrict: 'E',
				template,
				scope: {
					caseItem: '&'
				}
			};
		}
		])
		.name;
