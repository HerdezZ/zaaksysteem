export default ( option, value ) => {
	return option.active
		|| value === option.value
		|| (value && typeof value === 'object' && option.value in value);
};
