import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import rwdServiceModule from './../../../../../../shared/util/rwdService';
import observableStateParamsModule from './../../../../../../shared/util/route/observableStateParams';
import zsCasePhaseSidebarTabListModule from './zsCasePhaseSidebarTabList';
import zsCaseTasksModule from './zsCaseTasks';
import zsCaseActionListModule from './zsCaseActionList';
import controller from './CasePhaseSidebarController';
import './styles.scss';

export default angular
	.module('zsCasePhaseSidebar', [
		composedReducerModule,
		rwdServiceModule,
		zsCasePhaseSidebarTabListModule,
		zsCaseTasksModule,
		zsCaseActionListModule,
		observableStateParamsModule
	])
	.component('zsCasePhaseSidebar', {
		bindings: {
			caseId: '&',
			caseUuid: '&',
			actions: '&',
			actionsLoading: '&',
			checklist: '&',
			checklistLoading: '&',
			checklistResource: '&',
			tab: '&',
			isCollapsed: '&',
			phaseState: '&',
			onActionAutomaticToggle: '&',
			onActionUntaint: '&',
			onActionTrigger: '&',
			requestor: '&',
			templates: '&',
			caseDocuments: '&',
			phases: '&',
			selectedMilestone: '&',
			canEdit: '&'
		},
		controller,
		template
	})
	.name;
