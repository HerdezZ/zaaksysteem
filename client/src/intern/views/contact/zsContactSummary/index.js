import angular from 'angular';
import controller from './controller';
import template from './template.html';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import './styles.scss';

controller.$inject = [ '$scope', '$sce', 'composedReducer' ];

export default
	angular.module('zsContactSummary', [
		composedReducerModule
	])
		.component('zsContactSummary', {
			bindings: {
				subject: '&',
				isCollapsed: '&'
			},
			template,
			controller
		})
		.name;
