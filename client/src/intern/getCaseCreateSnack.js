import propCheck from './../shared/util/propCheck';

export default ( values = {}, actions = [] ) => {
  propCheck.throw(
    propCheck.shape({
      caseId: propCheck.number.optional,
      status: propCheck.string.optional,
    }),
    values
  );

  const { status, caseId } = values;

  let message = status === 'open' ?
    'Zaak is door u in behandeling genomen'
    : 'Zaak is geregistreerd';

  if (caseId && actions.length === 0) {
    message += ` onder zaaknummer ${caseId}`;
  }

  return {
    message,
    actions,
  };
};
