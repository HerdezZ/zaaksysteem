import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import rwdServiceModule from '../../../../shared/util/rwdService';
import appServiceModule from '../../appService';
import auxiliaryRouteModule from '../../../../shared/util/route/auxiliaryRoute';
import controller from './ProposalItemListController';
import template from './template.html';
import './styles.scss';

export default angular
	.module('Zaaksysteem.meeting.meetingListItem.proposalItemList', [
		composedReducerModule,
		rwdServiceModule,
		appServiceModule,
		angularUiRouterModule,
		auxiliaryRouteModule
	])
	.component('proposalItemList', {
		bindings: {
			proposals: '&',
			appConfig: '&'
		},
		controller,
		template
	})
	.name;
