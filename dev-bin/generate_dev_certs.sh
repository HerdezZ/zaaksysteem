#!/bin/sh

set -e
cd /etc/nginx/ssl

if [ ! -e /etc/nginx/ssl/openssl.cnf ]; then
    cat > openssl.cnf <<'EOT'
[ req ]
default_bits            = 2048
distinguished_name      = req_distinguished_name

[ req_distinguished_name ]
commonName                      = Common Name

[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:true
keyUsage = critical,digitalSignature,cRLSign,keyCertSign

[ server_cert ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:false
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth,clientAuth
subjectAltName = @alt_names

[alt_names]
DNS.1 = dev.zaaksysteem.nl
DNS.2 = *.dev.zaaksysteem.nl
EOT
fi

if [ ! -e /etc/nginx/ssl/ca.key ]; then
    echo "CA key does not exist. Generating a new one."
    openssl genrsa -out ca.key 2048
    openssl req \
        -config openssl.cnf \
        -extensions v3_ca \
        -new \
        -x509 \
        -days 3650 \
        -sha256 \
        -key ca.key \
        -out ca.crt \
        -subj "/CN=Zaaksysteem Development CA"
fi

if [ ! -e server.key ]; then
    echo "Generating new server key."
    openssl genrsa -out server.key 2048
fi

echo "Generating new server certificate (30 day validity)"
openssl req \
    -config openssl.cnf \
    -extensions server_cert \
    -new \
    -subj "/CN=dev.zaaksysteem.nl" \
    -key server.key \
    -out server.csr

openssl x509 \
    -extfile openssl.cnf \
    -extensions server_cert \
    -req \
    -days 30 \
    -sha256 \
    -in server.csr \
    -CA ca.crt \
    -CAkey ca.key \
    -set_serial "$(shuf -i 2000-65000 -n1)" \
    -out server-only.crt

cat server-only.crt ca.crt > server.crt
