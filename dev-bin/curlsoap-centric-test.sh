#!/bin/sh

if [ "$1" = "" ]; then
    echo "USAGE $0 xmlfile.xml";
    exit
fi

curl -kvv --data-binary @$1  -H "SOAPAction: http://www.egem.nl/StUF/sector/bg/0310/npsLv01" -H "Content-Type: text/xml;charset=UTF-8" http://10.15.4.139:83/StUF3.DDS.TEST/ServiceBeantwoordVraag.svc
