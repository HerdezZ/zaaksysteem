#!/usr/bin/env perl

use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::Config;
use Pod::Usage;
use File::Spec::Functions;
use File::Basename;
use Digest::SHA;
use MIME::Base64;
use Net::LDAP;
use BTTW::Tools qw/throw/;

use Encode qw/decode_utf8/;

use Time::HiRes qw(gettimeofday tv_interval);

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_AUTHORIZATION_ROLES/;

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    n          => 0,
);


{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                n
                config=s
                customer_d=s
                hostname=s
                organization=s
                skip_root=s
                ldapserver=s
                ldapuser=s
                ldappassword=s
                ldapbase=s
                )
        );
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config hostname organization ldapserver ldapuser ldappassword ldapbase)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

$opt{password} = 'zaaksysteem' unless $opt{password};

if (!defined $opt{customer_d}) {
    $opt{customer_d} = catdir(dirname($opt{config}), 'customer.d');
}

my $ZS = Zaaksysteem::Config->new(
    zs_customer_d => $opt{customer_d},
    zs_conf       => $opt{config},
);

my $schema = $ZS->get_customer_schema($opt{hostname}, 1);

my $interface   = $schema->resultset('Interface')->find_by_module_name('authldap');

my ($role_dn_to_id, $group_dn_to_id, $role_id_to_old_id, $group_id_to_old_id) = ({},{}, {}, {});


my @SYSTEM_ROLES = qw/
    Administrator
    Zaaksysteembeheerder
    Zaaktypebeheerder
    Zaakbeheerder
    Contactbeheerder
    Basisregistratiebeheerder
    Wethouder
    Directielid
    Afdelingshoofd
    Kcc-medewerker
    Zaakverdeler
    Behandelaar
    Gebruikersbeheerder
/;


my $roles = ZAAKSYSTEEM_AUTHORIZATION_ROLES;

my $ldaproles = {};
for my $role (keys %{ $roles }) {
    my $data    = $roles->{ $role };

    $ldaproles->{ $data->{ldapname} } = $data;
    $ldaproles->{ $data->{ldapname} }->{
        'internalname'
    } = $role;
}

my $subject_counter;

migrate_roles_and_groups($schema);

sub migrate_roles_and_groups {
    my $dbic    = shift;

    my $tree    = get_tree_view(
        {
            objectClass => ['organization', 'organizationalUnit','posixGroup','posixAccount']
        }
    );


    # print STDERR 'TREE: ' . Data::Dumper::Dumper($tree);
    do_transaction(
        $dbic,
        sub {

            my $parent;
            if (!$opt{skip_root}) {
                $parent      = $dbic->resultset('Groups')->create_group(
                    {
                        name        => ucfirst($opt{organization}),
                        description => ucfirst($opt{organization}) . ' root',
                    }
                );
            }

            create_group_and_roles_from_tree($dbic, $tree->[0], $parent, 1);
            create_group_and_roles_from_tree($dbic, $tree->[0], $parent, 1, 'posixAccount');

            create_missing_system_roles($dbic);

            if (my @errors = check_duplicate_ids($role_dn_to_id)) {
                die(
                    "Duplicate id errors:\n" . join('\n- ', @errors) . "\n"
                );
            }

            if (my @errors = check_duplicate_ids($group_dn_to_id)) {
                die(
                    "Duplicate id errors:\n" . join('\n- ', @errors) . "\n"
                );
            }

            print STDERR "Loaded subjects: " . $subject_counter . "\n";

            # update_ou_role($dbic);
        }
    );
}

sub check_duplicate_ids {
    my $source    = shift;

    my @errors;
    my %found;
    for my $dn (keys %{ $source }) {
        my $id = $source->{ $dn };
        print "Checking $dn: " . $id . " for duplicate ID\n";

        if ($found{$id}) {
            push(
                @errors,
                'Duplicate ids: ' . $found{$id} . ' with ' . $dn
            );
            next;
        }

        $found{$id} = $dn;
    }

    return @errors;
}

# sub update_ou_role {
#     my $dbic                = shift;

#     for my $new_id (keys %{ $group_id_to_old_id }) {
#         my $old_id  = $group_id_to_old_id->{ $new_id };

#         my $rs      = $dbic->schema->resultset('Zaak')->search(
#             {
#                 route_ou    => $old_id   
#             }
#         );

#         $rs->update({ route_ou => $new_id });

#         my $rs_zt   = $dbic->schema->resultset('ZaaktypeAuthorisation')->search(
#             {
#                 ou_id    => $old_id   
#             }
#         );

#         $rs_zt->update({ ou_id => $new_id });

#         my $rs_zt_rel   = $dbic->schema->resultset('ZaaktypeRelatie')->search(
#             {
#                 ou_id    => $old_id   
#             }
#         );

#         $rs_zt_rel->update({ ou_id => $new_id });
#     }

#     for my $new_id (keys %{ $role_id_to_old_id }) {
#         my $old_id  = $role_id_to_old_id->{ $new_id };

#         my $rs      = $dbic->schema->resultset('Zaak')->search(
#             {
#                 route_role    => $old_id   
#             }
#         );

#         $rs->update({ route_role => $new_id });

#         my $rs_zt   = $dbic->schema->resultset('ZaaktypeAuthorisation')->search(
#             {
#                 role_id    => $old_id   
#             }
#         );

#         $rs_zt->update({ role_id => $new_id });

#         my $rs_zt_rel   = $dbic->schema->resultset('ZaaktypeRelatie')->search(
#             {
#                 role_id    => $old_id   
#             }
#         );

#         $rs_zt_rel->update({ role_id => $new_id });
#     }
# }


sub create_group_and_roles_from_tree {
    my $dbic    = shift;
    my $tree    = shift;
    my $parent  = shift;
    my $depth   = shift;
    my $only_group = shift;

    my $leaf    = create_leaf($dbic, $tree, $parent, $depth, $only_group);

    for my $child (@{ $tree->{children} }) {
        create_group_and_roles_from_tree($dbic, $child, $leaf, ($depth+1), $only_group);
    }
}

sub create_leaf {
    my $dbic    = shift;
    my $tree    = shift;
    my $parent  = shift;
    my $depth   = shift;
    my $only_group = shift;

    # my ($role_dn_to_id, $group_dn_to_id, $role_id_to_old_id, $group_id_to_old_id) = ({},{}, {}, {});


    my $leaf = $parent;
    if (
        (
            !$only_group || $only_group eq 'organizationalUnit'
        ) &&
        $tree->{type} eq 'organizationalUnit'
    ) {
        print STDERR "Creating group: " . $tree->{entry}->{description} . '[' . $tree->{dn} . "]\n";
        $leaf   = $dbic->resultset('Groups')->create_group(
            {
                name        => $tree->{entry}->{description},
                description => $tree->{entry}->{description},
                $parent ? (parent_group_id => $parent->id) : (),
                override_id => $tree->{internal_id},
            }
        );

        print STDERR "  ID: " . $leaf->id . "\n";


        $group_dn_to_id->{ $tree->{dn} } = $leaf->id;
        $group_id_to_old_id->{ $leaf->id } = $tree->{internal_id};
    }

    if (
        (
            !$only_group || $only_group eq 'posixGroup'
        ) &&
        $tree->{type} eq 'posixGroup'
    ) {
        my $system;
        if (
            $depth == 2 &&
            grep(
                { $_ eq $tree->{name} } @SYSTEM_ROLES
            )
        ) {
            $system = 1;
        }

        print STDERR "Creating role: " . $tree->{name} . '[' . $tree->{dn} . "]\n";

        my $role = $dbic->resultset('Roles')->create_role(
            {
                override_id => $tree->{internal_id},
                name        => $tree->{name},
                description => $tree->{entry}->{description},
                $parent ? (parent_group_id => $parent->id) : (),
                $system ? (system_role => 1) : ()
            }
        );

        print STDERR "  ID: " . $role->id . "\n";


        $role_dn_to_id->{ $tree->{dn} } = $role->id;

        $role_id_to_old_id->{ $leaf->id } = $tree->{internal_id};
    }

    if (
        $only_group && 
        $only_group eq 'posixAccount' &&
        $tree->{type} eq 'posixAccount'
    ) {
        my $groupdn = $tree->{dn};
        $groupdn =~ s/^cn[^,]+,//;

        print STDERR "Creating/updating subject: " . $tree->{name} . '[' . $tree->{dn} . "]\n";
        if ($group_dn_to_id->{ $groupdn }) {
            print STDERR "  groups: " . $groupdn . ':' . $group_dn_to_id->{ $groupdn } . "\n";
        } else {
            print STDERR "  HIBBEM group: " . $groupdn . ':' . $group_dn_to_id->{ $groupdn } . "\n";
        }

        if ($tree->{entry}->{roles} && scalar keys %{ $tree->{entry}->{roles} }) {
            print STDERR "  roles: " . join(',', keys %{ $tree->{entry}->{roles} }) . "\n";
        } else {
            print STDERR "  HIBBEM roles: " . join(',', keys %{ $tree->{entry}->{roles} }) . "\n";
        }

        create_subject(
            $dbic,
            {
                username        => $tree->{name},
                cn              => $tree->{name},
                displayname     => $tree->{entry}->{displayName},
                givenname       => $tree->{entry}->{givenName},
                mail            => $tree->{entry}->{mail},
                telephonenumber => $tree->{entry}->{telephoneNumber},
                uid             => $tree->{name},
                sn              => $tree->{entry}->{sn},
                initials        => $tree->{entry}->{initials},
                password        => $tree->{entry}->{userPassword},
                roles           => $tree->{entry}->{roles},
            },
            $group_dn_to_id->{ $groupdn }
        )


    }

    return $leaf;
}

sub create_missing_system_roles {
    my $dbic            = shift;

    my $root_group      = $dbic->resultset('Groups')->search(
        {},
        {
            order_by    => { '-asc' => [\"array_length(me.path, 1)"] },
            rows        => 1
        }
    )->first;

    print STDERR "Creating missing system roles below: " . $root_group->name . "\n";

    for my $role (@SYSTEM_ROLES) {
        next if $dbic->resultset('Roles')->search(
            {
                system_role => 1,
                name        => $role,
            }
        )->count;

        print STDERR "  Adding missing system role: " . $role . "\n";

        $dbic->resultset('Roles')->create_role({
            name        => $role,
            description => $role,
            parent_group_id => $root_group->id,
            system_role => 1
        });
    }

    print STDERR "  [DONE]\n";
}


sub create_subject {
    my $dbic        = shift;
    my $userdata    = shift;
    my $group       = shift;

    ++$subject_counter;

    my @LDAPCOLS = qw/cn displayname givenname mail telephonenumber uid sn initials/;

    my $username    = $userdata->{username};

    my $ldap_data = {
        map({ $_ => $userdata->{ $_ } } @LDAPCOLS)
    };

    my $userentities  = $dbic->resultset('UserEntity')->search(
        {
            source_identifier   => $username,
            source_interface_id => $interface->id,
        }
    );

    my $subject;
    if ($userentities->count) {
        my $userentity = $userentities->first;

        $subject    = $userentity->subject_id;

        $userentity->password($userdata->{password});
        $userentity->update;

        $subject->properties($ldap_data);
    } else {
        $subject    = $dbic->resultset('Subject')->create(
            {
                subject_type    => 'employee',
                properties      => $ldap_data,
                username        => $username,
            }
        );


        $dbic->resultset('UserEntity')->create(
            {
                source_interface_id         => $interface->id,
                source_identifier           => $username,
                active                      => 1,
                subject_id                  => $subject->id,
                password                    => $userdata->{password}
            }
        );
    }

    $subject->group_ids([$group]);

    if ($userdata->{roles}) {
        my @role_ids;

        for my $role (keys %{ $userdata->{roles} }) {
            push(@role_ids, $role_dn_to_id->{$role});
        }

        $subject->role_ids(\@role_ids);
    }

    

    $subject->update;

    warn("Created user: " . $username . "\n");
    # warn("  Group: " . $group->name . "\n");
    # warn("  Roles:\n    - " . join("\n    - ", map { $_->name } @$roles) . "\n");

    return $subject;
}

sub do_transaction {
    my ($dbic, $sub) = @_;

    $dbic->txn_do(
        sub {
            $sub->();
            if ($opt{n}) {
                $dbic->txn_rollback;
            }
        }
    );
}

sub password_ssha {
    my $pass    = shift;

    my ($hashedPasswd,$salt);

    $salt           = get_salt8();
    my $ctx         = Digest::SHA->new;

    $ctx->add($pass);
    $ctx->add($salt);

    $hashedPasswd   = '{SSHA}' . encode_base64($ctx->digest . $salt,'');

    return($hashedPasswd);
}

sub get_salt8 {
    my $salt = join '', ('a'..'z')[rand 26,rand 26,rand 26,rand 26,rand 26,rand 26,rand 26,rand 26];
    return($salt);
}

my $ldaph;
sub ldaph {

    return $ldaph if $ldaph;

    my $ldap_server = $opt{ldapserver};
    my $ldap_port   = 389;

    my $ldap = Net::LDAP->new(
        "$ldap_server:$ldap_port",
        version => 3
      )
      or throw(
        '/Zaaksysteem/Users/ldaph',
        sprintf(
            "Unable to connect to LDAP server %s:%d", $ldap_server, $ldap_port
        ));

    my $msg = $ldap->bind(
        $opt{ldapuser},
        password => $opt{ldappassword}
    );

    if ($msg->code) {
        throw(
            '/Zaaksysteem/Users/ldaph',
            sprintf("LDAP error: %s [%s]", $msg->error, $msg->code),
        );
    }

    return $ldaph = $ldap;
}

sub get_tree_view {
    my $opts    = shift || {};
    my $parent  = shift;
    my $ldap    = ldaph();
    my $rv      = [];

    my @objectClass = (
        $opts->{objectClass}
            ? @{ $opts->{objectClass } }
            : ('organization', 'posixAccount','organizationalUnit','posixGroup')
        );

    my $filter = '(|(objectClass='
            . join(')(objectClass=', @objectClass)
            . '))';

    my $sh      = $ldap->search( # perform a search
        base        => ($parent || $opt{ldapbase}),
        scope       => ($parent ? 'one' : 'base'),
        filter      => $filter
    );

    #$self->log->debug('COUNT: ' . $sh->count . ' / ' . ($parent ||
    #        $self->ldapbase) . ' / ' . $filter
    #);

    return $rv unless $sh->count;

    my @entries = $sh->entries;

    my @results;
    for my $entry (@entries) {
        my $layout = _load_tree_entry($entry, $opts);

        push(@results, $layout);
    }

    my @sorted_results = sort {
        $a->{type} cmp $b->{type} ||
        lc($a->{name}) cmp lc($b->{name})
    } @results;

    return \@sorted_results;
}

sub _load_tree_entry {
    my ($entry, $opts) = @_;

    my $layout = {
        name    => '',
        entry   => {},
        dn      => $entry->dn,
        has_children => (has_children($entry->dn) || 0),
        system  => 0
    };

    unless ($opts->{counter}) {
        $opts->{counter} = 0;
    }

    $layout->{counter}      = ++$opts->{counter};

    my @object_classes = $entry->get_value('objectClass');
    if (grep { $_ eq 'organization' } @object_classes) {
        $layout->{entry}        = get_organization($entry);
        $layout->{name}         = $layout->{entry}->{o};
        $layout->{type}         = 'organization';
        $layout->{internal_id}  = 0;
        $layout->{children}     = get_tree_view($opts, $entry->dn);
    } elsif (grep { $_ eq 'posixAccount' } @object_classes) {
        $layout->{entry}        = get_medewerker($entry);
        $layout->{name}         = $layout->{entry}->{cn};
        $layout->{type}         = 'posixAccount';
        $layout->{internal_id}  = $layout->{entry}->{uidNumber};
        $layout->{children}     = [];
    } elsif (grep { $_ eq 'organizationalUnit' } @object_classes) {
        $layout->{entry}        = get_organisational_unit($entry);
        $layout->{name}         = $layout->{entry}->{ou};
        $layout->{type}         = 'organizationalUnit';
        $layout->{children}     = get_tree_view($opts, $entry->dn);
        $layout->{internal_id}  = $layout->{entry}->{l};
    } elsif (grep { $_ eq 'posixGroup' } @object_classes) {
        $layout->{entry}        = get_posix_group($entry);
        $layout->{name}         = $layout->{entry}->{cn};
        $layout->{type}         = 'posixGroup';
        $layout->{members}      = $layout->{entry}->{memberUid};
        $layout->{has_children} = has_children($entry->dn);
        $layout->{internal_id}  = $layout->{entry}->{gidNumber};
        $layout->{children}     = [];
    }

    if ($layout->{type} eq 'posixGroup') {
        #$self->log->debug(
        #    'dn: ' . $entry->dn . ' / ' . $layout->{name}
        #);
    }
    if (
        $layout->{type} eq 'posixGroup' &&
        $entry->dn !~ /ou/ &&
        $ldaproles->{ $layout->{name} }
    ) {
        $layout->{system} = 1;
    }

    return $layout;
}

sub has_children {
    my ($dn) = @_;
    my $ldap    = ldaph;

    ### Check if there are entries below this object
    my $sh      = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'one',
        filter  => "(&(objectClass=*))"
    );

    return unless $sh->count;

    return $sh->count;
}

sub get_organization {
    my ($entry)  = @_;
    my $ldap            = ldaph;

    return unless $entry && ref($entry);

    return {
        o   => $entry->get_value('o')
    };
}

sub get_organisational_unit {
    my ($entry)  = @_;
    my $ldap            = ldaph;

    return unless $entry && ref($entry);

    my $ou      = {};
    my $dn      = $entry->dn;

    $ou->{ $_ } = $entry->get_value($_) for
        qw/ou description l/;

    return $ou;
}

sub get_posix_group {
    my ($entry)  = @_;
    my $ldap            = ldaph;

    return unless $entry && ref($entry);

    my $group      = {};
    my $dn      = $entry->dn;

    $group->{ $_ } = $entry->get_value($_) for
        qw/cn description gidNumber memberUid/;

    $group->{memberUid} = [ $entry->get_value('memberUid') ];

    return $group;
}

sub get_medewerker {
    my ($entry)  = @_;
    my $ldap            = $ldaph;

    return unless $entry && ref($entry);

    my $mw      = {};
    my $dn      = $entry->dn;

    $mw->{ $_ } = decode_utf8($entry->get_value($_)) for
    #$mw->{ $_ } = $entry->get_value($_) for
        qw/cn displayName uid uidNumber initials sn givenName mail telephoneNumber userPassword/;

    print STDERR "SURNAME: " . $mw->{sn} . "\n";

    ### Get OU
    my ($ou)    = $dn =~ /ou=(.*?),/;

    $mw->{ou}   = $ou;

    ### Get roles
    my $roles   = $ldap->search( # perform a search
        base   => $opt{ldapbase},
        filter => "(&(objectClass=posixGroup)(memberUid=$dn))",
    );

    my %roles;
    if ($roles->count) {
        for my $role ($roles->entries) {
            $roles{$role->dn} = 1;
        }
    }

    $mw->{roles}    = \%roles;

    return $mw;
}


1;

__END__

=head1 NAME

migrate_ldap_to_db.pl - A migration script migrating LDAP users/organizationunits and roles into the db

=head1 SYNOPSIS

migrate_ldap_to_db.pl OPTIONS --organization Mintlab

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * organization

Organization name, to use as primary root of the Organization Tree

=item * skip_root

Some LDAP customers already have a root OU, use that.

=item * ldapserver

    localhost

The LDAP server (hostname) to connect to

=item * ldapuser

    cn=admin,dc=zaaksysteem,dc=nl

The user to bind to LDAP with

=item * ldappassword

    AJASdsaDjsdj

Password to use when logging on to LDAP

=item * ldapbase

    o=zaaksysteem,dc=zaaksysteem,dc=nl

The customer base DN to get the users from

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
