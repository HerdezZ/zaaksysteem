package TestFor::Catalyst::Controller::API::V1::Search;
use base qw(ZSTest::Catalyst);

use Moose;
use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::Search - Search API for Dashboard

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Search.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/search> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API.

=head2 Search

=head3 search

Url: /api/v1/search

=cut

sub cat_api_v1_search : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my ($mech, $data) = ($zs->mech, undef);

        $zs->create_zaaktype_predefined_ok(naam => 'With a rrruaarr title');
        $zs->create_zaaktype_predefined_ok(naam => 'With another title');

        $mech->zs_login;

        {
            $mech->get(
                $mech->zs_url_base . '/api/v1/search/casetype?query=With',
            );

            $data   = $zs->get_json_as_perl($mech->content);

            is($data->{result}->{type}, 'set', 'Got a set of data');

            my $instance = $data->{result}->{instance};
            is(@{ $instance->{rows} }, 2, 'Got 2 rows of data');
        }

        {
            $mech->get(
                $mech->zs_url_base . '/api/v1/search/casetype?query=rrruaarr',
            );

            $data   = $zs->get_json_as_perl($mech->content);

            my $instance = $data->{result}->{instance};
            is(@{ $instance->{rows} }, 1, 'Got 1 row of data');
        }

        # is ($data->{result}->{instance}->{query}->{validity}, 'missing','Exception: no query given');

    }, 'api/v1/search: searches');

    $zs->zs_transaction_ok(sub {
        my ($mech, $data) = ($zs->mech, undef);

        my $caset1 = $zs->create_zaaktype_predefined_ok(naam => 'With a rrruaarr title');
        my $caset2 = $zs->create_zaaktype_predefined_ok(naam => 'With another title');

        $caset1->zaaktype_node_id->trigger('intern');
        $caset1->zaaktype_node_id->update;

        $mech->zs_login;

        {
            $mech->get(
                $mech->zs_url_base . '/api/v1/search/casetype?query=With&filter=trigger:intern',
            );

            $data   = $zs->get_json_as_perl($mech->content);

            diag(explain($mech->content));

            my $instance = $data->{result}->{instance};
            is(@{ $instance->{rows} }, 1, 'Got 1 row of data, filter trigger');
        }

        {
            $mech->get(
                $mech->zs_url_base . '/api/v1/search/casetype?query=With&filter=trigger:intern&filter=betrokkene_type:natuurlijk_persoon',
            );

            $data   = $zs->get_json_as_perl($mech->content);

            my $instance = $data->{result}->{instance};
            is(@{ $instance->{rows} }, 0, 'Got 0 rows of data, filter betrokkene_type and trigger');
        }

        # is ($data->{result}->{instance}->{query}->{validity}, 'missing','Exception: no query given');

    }, 'api/v1/search: searches with filter');

    $zs->zs_transaction_ok(sub {
        my ($mech, $data) = ($zs->mech, undef);

        $mech->zs_login;
        $mech->get(
            $mech->zs_url_base . '/api/v1/search',
        );

        $data   = $zs->get_json_as_perl($mech->content);

        is ($data->{result}->{instance}->{query}->{validity}, 'missing','Exception: no query given');

    }, 'api/v1/search: search exceptions');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
