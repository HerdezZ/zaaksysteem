package TestFor::General::Object::TypeModel;

# ./zs_prove -v t/lib/TestFor/General/Object/TypeModel.pm
use base 'ZSTest';

use TestSetup;

use Scalar::Util qw[refaddr];
use Data::UUID qw[];

use Zaaksysteem::Object::TypeModel;
use Zaaksysteem::Object::Model;
use Zaaksysteem::Object::Types::Type;

sub object_type_model : Tests {
    my $self = shift;

    my $model = Zaaksysteem::Object::TypeModel->new;

SKIP: {
    skip 'AUTUMN2015BREAK: db: Can\'t locate object method "name" via package "Moose::Meta::Class::__ANON__::SERIAL::26"',3;

    $zs->txn_ok(sub {
        my $attr = $zs->create_bibliotheek_kenmerk_ok(
            magic_string => 'name',
            value_type => 'text'
        );

        my $type = Zaaksysteem::Object::Types::Type->new(
            name => 'UserDefinedType',
            category_id => 0,
            prefix => 'udt',
            attributes => [
                {
                    index => 0,
                    required => 0,
                    attribute_type => 'Name',
                    attribute_label => 'Name',
                    attribute_id => $attr->id,
                    attribute_object => $attr
                }
            ]
        );

        my $meta = $type->instance_meta_class;

        isa_ok($meta, 'Class::MOP::Class', 'returned meta instance is a meta class instance');

        my $instance = $meta->new_object(name => 'somename');

        isa_ok($instance, 'Zaaksysteem::Object', 'metaclass creates objects that derive from Zaaksysteem::Object');

        can_ok($instance, 'name');

        is $instance->name, 'somename', 'object instantiates with correct value for name';

        my $values = $instance->TO_JSON->{ values };

        isa_ok $values, 'HASH', 'Values key exists in JSON body and is a hash';

        is $values->{ name }, 'somename', 'name property is correctly serialized (implying the OA trait is set)';
    }, 'Base tests for ObjectType model ok');

}; # END SKIP

    $zs->txn_ok(sub {
        my $attr = $zs->create_bibliotheek_kenmerk_ok;

        my $type = Zaaksysteem::Object::Types::Type->new(
            id => Data::UUID->new->create_str,
            name => 'UserDefinedType',
            category_id => 0,
            prefix => 'udt',
            attributes => []
        );

        my $meta = $type->instance_meta_class;
        my $meta2 = $model->get_meta_class(meta_type => $type);
        my $meta3 = $model->get_meta_class(meta_type => $type);

        isa_ok $meta, 'Moose::Meta::Class', 'instance_meta_class returns a Moose::Meta::Class';
        isa_ok $meta2, 'Moose::Meta::Class', 'get_meta_class returns a Moose::Meta::Class';
        isa_ok $meta3, 'Moose::Meta::Class', 'get_meta_class returns a Moose::Meta::Class on succesive call';

        isnt $meta, $meta2, 'build_meta_class and first get_meta_class return different objects';
        is $meta2, $meta3, 'successive get_meta_class calls return the same object';
        isnt $meta, $meta3, 'test is sane, build_meta_class and second get_meta_class invocation also return different objects';
    }, 'ObjectType cache works ok');
}

sub object_type_model_usage : Tests {
    my $self = shift;

    my $model = Zaaksysteem::Object::TypeModel->new;

    my $type = Zaaksysteem::Object::Types::Type->new(
        id => Data::UUID->new->create_str,
        name => 'UserDefinedType',
        category_id => 0,
        prefix => 'udt',
        attributes => []
    );

    my $type2 = Zaaksysteem::Object::Types::Type->new(
        id => Data::UUID->new->create_str,
        name => 'SecondDefinedType',
        category_id => 0,
        prefix => 'sdt',
        attributes => []
    );

    $model->cache_meta_types($type, $type2);

    my $object1a = $model->new_object('udt');
    my $object1b = $model->new_object('udt');
    my $object2a = $model->new_object('sdt');
    my $object2b = $model->new_object('sdt');

    isnt refaddr $object1a, refaddr $object1b, 'New instances of object returned on successive calls of new_object';
    is refaddr $object1a->meta, refaddr $object1b->meta, 'New instances of same type share same meta object';
    isnt refaddr $object1a->meta, refaddr $object2a->meta, 'New instances of different type do not share meta object';
}

sub object_type_model_attrs : Tests {
    my $self = shift;

    my $model = Zaaksysteem::Object::TypeModel->new;

    my $type = Zaaksysteem::Object::Types::Type->new(
        name => 'UserDefinedType',
        category_id => 0,
        prefix => 'udt',
        attributes => [
            {
                index => 0,
                required => 1,
                attribute_type => 'name',
                attribute_label => 'name',
                attribute_id => 1
            }
        ]
    );


SKIP: {
    skip 'AUTUMN2015BREAK: warnings and no die',3;

    $model->cache_meta_types($type);

    dies_ok { $model->new_object('udt') } 'instantiating a user defined type with a required attribute without that attribute dies';

    lives_ok { $model->new_object('udt', name => 'myname') } 'instantiating a user defined type with a required attribute works';

}; # END SKIP

}

sub object_type_case : Tests {
    my $self = shift;

    return;

    $zs->txn_ok(sub {
        my $model = $zs->create_object_model_ok;

        my $object_type = $zs->create_object_type_ok(model => $model);
        my $meta_class = $model->type_model->get_meta_class(object_type => $object_type);

        my $casetype = $zs->create_zaaktype_ok;

        my $casetype_attr = $zs->create_zaaktype_kenmerk_ok(
            node        => $casetype->zaaktype_node_id,
            object_type => $object_type
        );

        my $case = $zs->create_case_ok(zaaktype => $casetype);

        my $object1 = $model->save(object => $meta_class->new_object);

        # Add the hard relation to a object
        $case->object_data->object_relationships_object1_uuids->create({
            object1_uuid => $case->object_data->uuid,
            object1_type => $case->object_data->object_class,
            type1        => 'embeds',

            object2_uuid => $object1->id,
            object2_type => $object1->type,
            type2        => 'embedded'
        });

        $case->_clear_object_data;

    }, 'simple case of user defined object instances and case interaction');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
