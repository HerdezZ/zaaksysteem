#! perl

### Test header start
#use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end


$zs->zs_transaction_ok(sub {
    my $subject = $zs->create_subject_ok;

    throws_ok(sub {
        $subject->remove_inavigator_settings;
    }, qr/Zaaktypecode is verplicht/, 'Fails when no code is given');

    my $settings = {
        categories => {
            1 => 45,
            2 => 46,
        },
        document => {
            3 => 47,
            1 => 48
        }
    };

    $subject->modify_setting('inavigator_settings', $settings);
    is_deeply $subject->settings->{inavigator_settings}, $settings, "Settings are stored correctly";
    $subject->remove_inavigator_settings(1);

    $settings = {
        categories => {
            2 => 46,
        },
        document => {
            3 => 47,
        }
    };

    is_deeply $subject->settings->{inavigator_settings}, $settings, "Cleaning weeded out selection";
    $subject->remove_inavigator_settings(3);

    $settings = {
        categories => {
            2 => 46,
        },
        document => {
        }
    };

    is_deeply $subject->settings->{inavigator_settings}, $settings, "Cleaning weeded out selection once again";


}, 'Subject settings');

zs_done_testing();