#! perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::TestUtils;

use Zaaksysteem::Constants qw(
  SJABLONEN_TARGET_FORMATS
  ZAAK_CREATE_PROFILE_BETROKKENE
  PROFILE_BAG_TYPES_OK
  BAG_TYPES
  VALID_FQDN
  ZS_PROFILE_CREATE_DOMAIN
);

{
    note("SJABLONEN_TARGET_FORMATS tests");
    my %tests = (
        pdf => 1,
        odt => 1,
        jpg => 0,
    );

    foreach my $format (sort keys %tests) {
        my $res = SJABLONEN_TARGET_FORMATS->($format);
        is(
            $res,
            $tests{$format},
            sprintf(
                "%s is a%s %scorrect target format",
                $format,
                $tests{$format} ? '' : 'n',
                $tests{$format} ? '' : 'in'
            ));
    }
}


{
    note("ZAAK_CREATE_PROFILE_BETROKKENE tests");

    my %tests = (
        blessed => {
            betrokkene => bless({}, "Betrokkene"),
            rc         => 1
        },
        hash => {
            betrokkene => {
                betrokkene      => 1,
                create          => 1,
                betrokkene_type => 1,
                betrokkene_id   => 1,
                verificatie     => 1
            },
            rc => 1
        },
        no_verification => {
            betrokkene => {
                betrokkene      => 1,
                create          => 1,
                betrokkene_type => 1,
                betrokkene_id   => 1,
                verificatie     => 0,
            },
            rc => 0
        },
        create_has_no_type => {
            betrokkene => {
                betrokkene      => 0,
                create          => 1,
                betrokkene_id   => 1,
                betrokkene_type => 0
            },
            rc => 0
        },
        no_betrokkene_with_id_type => {
            betrokkene => {
                betrokkene      => 0,
                create          => 1,
                betrokkene_id   => 1,
                betrokkene_type => 1,
                verificatie     => 1,
            },
            rc => 1
        },
        no_betrokkene_type_no_id => {
            betrokkene => {
                betrokkene      => 0,
                create          => 1,
                betrokkene_id   => 0,
                betrokkene_type => 1,
                verificatie     => 1,
            },
            rc => 1
        },
        no_create_type_no_id => {
            betrokkene => {
                betrokkene      => 1,
                create          => 0,
                betrokkene_id   => 0,
                betrokkene_type => 1,
                verificatie     => 1,
            },
            rc => 1
        },

        array_ok => {
            betrokkene => [
                bless({}, "Betrokkene"), {
                    betrokkene      => 1,
                    create          => 1,
                    betrokkene_type => 1,
                    betrokkene_id   => 1,
                    verificatie     => 1
                },
            ],
            rc => 1
        },
        scalar => {
            betrokkene => "1",
            rc => 0
        },
    );

    for my $key (keys %tests) {
        my $ok = ZAAK_CREATE_PROFILE_BETROKKENE->($tests{$key}{betrokkene});
        is($ok, $tests{$key}{rc}, "$key: Correct return code");
    }
}

{
    note("BAG");
    my @bag_types = @{BAG_TYPES()};
    foreach (@bag_types) {
        ok(PROFILE_BAG_TYPES_OK->($_), "BAG type $_ is correct");
    }
    foreach (qw(invalid meuk)) {
        ok(!PROFILE_BAG_TYPES_OK->($_), "BAG type $_ is not correct");
    }
}

{
    note("VALID_FQDN");
    my %dns_labels = (
        'zaaksysteem.nl'           => 1,
        'zaaksysteem.nl.'          => 1,
        'hostname.zaaksysteem.nl'  => 1,
        '1b.zaaksysteem.nl'        => 1,
        'a--a.zaaksysteem.nl'      => 1,
        '1.zaaksysteem.nl'         => 1,
        'a.zaaksysteem.nl'         => 1,
        'a-a.zaaksysteem.nl'       => 1,
    Zaaksysteem::TestUtils::generate_random_string . ".zaaksysteem.nl" => 1,

        '*.invalid.zaaksysteem.nl' => 0,    # valid label, but not an FQDN
        '.nl'                      => 0,
        '.'                        => 0,
        'x' x 66 . '.invalid.zaaksysteem.nl'  => 0,
        'x.' x 125 . 'invalid.zaaksysteem.nl' => 0,
    );

    foreach (sort keys %dns_labels) {
        is(VALID_FQDN->($_), $dns_labels{$_}, "Valid FQDN check for '$_'");
    }

}

zs_done_testing();
