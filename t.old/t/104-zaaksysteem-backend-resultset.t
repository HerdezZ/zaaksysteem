#! perl
use TestSetup;
initialize_test_globals_ok;

$zs->zs_transaction_ok(
    sub {
        my $rs_active = $schema->resultset('Zaaktype')->search_active;
        ok(!$rs_active->count, 'Found no active casetypes');

        $zs->create_zaaktype_ok(active => 1);

        $rs_active = $schema->resultset('Zaaktype')->search_active;
        is($rs_active->count, 1, 'Found active casetypes');

        my $active = $rs_active->first;
        $active->active(0);
        $active->update;

        $rs_active = $schema->resultset('Zaaktype')->search_active;

        ok(!$rs_active->count, 'Correctly removed active casetypes');
    },
    'Tested search_active'
);

$zs->zs_transaction_ok(
    sub {

        my $resultset = $schema->resultset('Zaaktype');
        $zs->create_zaaktype_ok(titel => 'test', active => 1);

        ok($resultset->search_freeform('test')->count,
            'Found casetypes matching test');

        ok(
            !$resultset->search_freeform('bla$#Fn2983at')->count,
            'Did not find casetypes matching bla$#Fn2983at'
        );

        ok(
            $resultset->search_freeform('TeSt')->count,
            'Correct case insensitive search for TeSt'
        );

        ok($resultset->search_freeform('     test      ')->count,
            'Correct search with too much spaces');
    },
    'Tested search_freeform'
);

zs_done_testing();
