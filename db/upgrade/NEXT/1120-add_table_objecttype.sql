
BEGIN;

DROP INDEX IF EXISTS objecttype_version_uuid_idx;
DROP INDEX IF EXISTS objecttype_version_date_deleted_idx;
DROP INDEX IF EXISTS objecttype_uuid_idx;
DROP TABLE IF EXISTS objecttype_version CASCADE;
DROP TYPE IF EXISTS objecttype_status;
DROP TABLE IF EXISTS objecttype_version_custom_fields;
DROP TRIGGER IF EXISTS objecttype_version_update_trigger ON objecttype_version;
DROP TABLE IF EXISTS objecttype;

CREATE TYPE objecttype_status AS ENUM ('active','inactive');

CREATE TABLE objecttype_version (
    id SERIAL PRIMARY KEY,
    uuid uuid UNIQUE NOT NULL,
    name TEXT,
    title TEXT,
    status objecttype_status NOT NULL DEFAULT 'active',
    version INTEGER NOT NULL,
    custom_field_definition JSONB,
    authorizations JSONB,
    relationship_definition JSONB,
    date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
    last_modified TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
    date_deleted TIMESTAMP WITHOUT TIME ZONE
);

CREATE INDEX objecttype_version_uuid_idx ON objecttype_version(uuid);
CREATE INDEX objecttype_version_date_deleted_idx ON objecttype_version(date_deleted);

CREATE TRIGGER objecttype_version_update_trigger
    BEFORE UPDATE ON objecttype_version
    FOR EACH ROW
    EXECUTE PROCEDURE update_timestamps();

CREATE TABLE objecttype (
    id SERIAL PRIMARY KEY,
    uuid uuid UNIQUE NOT NULL,
    objecttype_version_id INTEGER REFERENCES objecttype_version(id)
);

CREATE INDEX objecttype_uuid_idx ON objecttype(uuid);

ALTER TABLE objecttype_version ADD COLUMN objecttype_id INTEGER REFERENCES objecttype(id);

COMMIT;

-- INSERT INTO objecttype_version (uuid, name, title, status) VALUES(uuid_generate_v4(), 'Test1', 'Description of test1', 'active');
-- INSERT INTO objecttype_version (uuid, name, title, status) VALUES(uuid_generate_v4(), 'Test2', 'Description of test2', 'active');
-- INSERT INTO objecttype_version (uuid, name, title, status) VALUES(uuid_generate_v4(), 'Test3', 'Description of test3', 'active');
-- INSERT INTO objecttype_version (uuid, name, title, status) VALUES(uuid_generate_v4(), 'Test4', 'Description of test4', 'active');
-- INSERT INTO objecttype (uuid, objecttype_version_id) VALUES (uuid_generate_v4(), (SELECT id FROM objecttype_version LIMIT 1));
-- SELECT objecttype_version.uuid, objecttype_version.name, objecttype_version.title, objecttype_version.status, objecttype_version.date_created, objecttype_version.last_modified, objecttype_version.date_deleted FROM objecttype JOIN objecttype_version ON objecttype.objecttype_version_id = objecttype_version.id;
