BEGIN;

CREATE TABLE groups (
    id INTEGER PRIMARY KEY NOT NULL,
    path INTEGER[] NOT NULL,
    name TEXT,
    description TEXT,
    date_created timestamp without time zone,
    date_modified timestamp without time zone
);

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


CREATE TABLE roles (
    id SERIAL PRIMARY KEY NOT NULL,
    parent_group_id INTEGER REFERENCES groups(id),
    name TEXT,
    description TEXT,
    system_role BOOLEAN,
    date_created timestamp without time zone,
    date_modified timestamp without time zone
);

ALTER TABLE subject ADD COLUMN role_ids INTEGER[];
ALTER TABLE subject ADD COLUMN group_ids INTEGER[];

-- TESTS

-- INSERT INTO acl_group (id,path, name, description) values (nextval('acl_group_id_seq'), ARRAY[currval('acl_group_id_seq')], 'group 1', 'Group 1');
-- INSERT INTO acl_group (id,path, name, description) values (nextval('acl_group_id_seq'), ARRAY[currval('acl_group_id_seq')], 'group 2', 'Group 2');
-- INSERT INTO acl_group (id,path, name, description) values (nextval('acl_group_id_seq'), ARRAY[2, currval('acl_group_id_seq')], 'group 2.1', 'Group 2.1');

-- INSERT INTO subject (subject_type, username, role_ids) values('employee','testuser','{}', '{}')

COMMIT;
