# End-to-End tests

This is the last front-end part that does not run in a Docker container and 
requires a Node.js installation on the host.

Heads up: It is safer to have an empty `.babelrc` file in this directory,
because imports and exports are transpiled to ES2015 and babel will look 
for a config file up the directory tree; if it finds a bad configuration
or unresolved modules, the tests will fail.

ZS-TODO: refactor ES 2015 imports/exports as vanilla Node.js modules
         and remove the babel dependencies.
