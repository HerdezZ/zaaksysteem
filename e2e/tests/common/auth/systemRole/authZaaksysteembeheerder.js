import {
    openPage,
    openPageAs
} from './../../../../functions/common/navigate';

const restrictedPage = $('.no-access');
const pageData = [
    '/medewerker',
    '/beheer/logging',
    '/beheer/sysin/transactions',
    '/beheer/configuration'
];

describe('when logging in as behandelaar', () => {
    beforeAll(() => {
        openPageAs('behandelaar');
    });

    pageData.forEach(page => {
        describe(`and opening the "${page}" page`, () => {
            beforeAll(() => {
                browser.get(page);
            });

            it('should show the no-access page', () => {
                expect(restrictedPage.isPresent()).toBe(true);
            });
        });
    });
});

describe('when logging in as zaaksysteembeheerder', () => {
    beforeAll(() => {
        openPageAs('zaaksysteembeheerder');
    });

    pageData.forEach(page => {
        describe(`and opening the "${page}" page`, () => {
            beforeAll(() => {
                browser.get(page);
            });

            it('should not show the no-access page', () => {
                expect(restrictedPage.isPresent()).toBe(false);
            });
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
