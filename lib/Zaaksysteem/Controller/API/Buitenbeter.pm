package Zaaksysteem::Controller::API::Buitenbeter;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub load_interface : Private {
    my ($self, $c) = @_;

    $c->stash->{interface} = $c->model('DB::Interface')->search_active({
        module => 'buitenbeter'
    })->first or throw("api/buitenbeter", "Buitenbeter koppeling is niet beschikbaar");
}


# trigger the import in spoof mode, useful for development and clean demonstration/testing
sub spoof : Local : ZAPI {
    my ($self, $c) = @_;

    $c->forward('load_interface');

    my $test_responses = $c->stash->{interface}->process_trigger('get_test_responses');

    $c->stash->{zapi} = $c->stash->{interface}->process_trigger('GetNewMeldingen', {
        test_responses => $test_responses,
        spoof_time => $c->stash->{interface}->get_interface_config->{interval} * 60 * 15,
    }, {
        zaak => $c->model('Zaak')
    });
}


# manually trigger the actual import
sub GetNewMeldingen : Local : ZAPI {
    my ($self, $c) = @_;

    $c->forward('load_interface');

    $c->stash->{zapi} = $c->stash->{interface}->process_trigger('GetNewMeldingen', {}, {
        zaak => $c->model('Zaak')
    });
}


sub PostStatusUpdate : Local : ZAPI {
    my ($self, $c) = @_;

    $c->forward('load_interface');

    my $case_id = $c->req->params->{case_id};
    my $case = $c->model('DB::Zaak')->find($case_id)
        or throw('sysin/buitenbeter', "Zaak $case_id niet gevonden, systeemfout");

    my $kenmerken = $case->field_values;

    my $result = $c->stash->{interface}->process_trigger('PostStatusUpdate', {
        kenmerken => $kenmerken,
        statusCode => 'In behandeling',
        statusText => 'dit is de status text'
    });

    $c->stash->{zapi} = [$result];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 GetNewMeldingen

TODO: Fix the POD

=cut

=head2 PostStatusUpdate

TODO: Fix the POD

=cut

=head2 load_interface

TODO: Fix the POD

=cut

=head2 spoof

TODO: Fix the POD

=cut

