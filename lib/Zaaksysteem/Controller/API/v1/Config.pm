package Zaaksysteem::Controller::API::v1::Config;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Config - Resource endpoints for Zaaksysteem
configuration items

=head1 DESCRIPTION

This controller handles API interactions for objects in the C<config>
namespace (such as C<config/item> and C<config/definition>).

=cut

use BTTW::Tools;
use List::Util qw[first];
use Moose::Util::TypeConstraints qw(enum);
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Object::Syntax;
use Zaaksysteem::Types qw(UUID);

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/config> routing namepace.

=cut

sub base : Chained('/api/v1/base') : PathPart('config') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');
    $c->stash->{model} = $c->model('Config');
}

=head2 category_base

Reserves the C</api/v1/config/category> routing namespace.

=cut

sub category_base : Chained('base') : PathPart('category') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{categories} = $c->stash->{model}->category_store;
}

=head2 category_instance_base

Reserves the C</api/v1/config/category/[UUID]> routing namespace.

=cut

sub category_instance_base : Chained('category_base') : PathPart('')
    : CaptureArgs(1) {

    my ($self, $c, $category_id) = @_;

    my $category = $c->stash->{categories}->retrieve($category_id);

    unless (defined $category) {
        throw(
            'api/v1/config/category/not_found',
            sprintf('The config category with UUID "%s" could not be found.',
                $category_id),
            { http_code => 404 }
        );
    }

    $c->stash->{category} = $category;
}

=head2 definition_base

Reserves the C</api/v1/config/definition> routing namespace.

=cut

sub definition_base : Chained('base') : PathPart('definition')
    : CaptureArgs(0) {

    my ($self, $c) = @_;

    $c->stash->{definitions} = $c->stash->{model}->definition_store;
}

=head2 definition_instance_base

Reserves the C</api/v1/config/definition/[UUID]> routing namespace.

=cut

sub definition_instance_base : Chained('definition_base') : PathPart('')
    : CaptureArgs(1) {
    my ($self, $c, $config_definition_id) = @_;

    my $definition = $c->stash->{definitions}->retrieve($config_definition_id);

    unless (defined $definition) {
        throw(
            'api/v1/config/definition/not_found',
            sprintf('The config definition with UUID "%s" could not be found',
                $config_definition_id),
            { http_code => 404 }
        );
    }

    $c->stash->{definition} = $definition;
}

=head2 item_base

Reserves the C</api/v1/config/item> routing namespace.

=cut

sub item_base : Chained('base') : PathPart('item') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{items} = $c->stash->{model}->item_store;
}

=head2 item_instance_base

Reserves the C</api/v1/config/item/[UUID]> routing namespace.

=cut

sub item_instance_base : Chained('item_base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $config_item_id) = @_;

    my $item = $c->stash->{items}->retrieve($config_item_id);

    unless (defined $item) {
        throw(
            'api/v1/config/item/not_found',
            sprintf('The config item with UUID "%s" could not be found',
                $config_item_id),
            { http_code => 404 }
        );
    }

    $c->stash->{item} = $item;
}

=head2 category_get

=head2 URL Path

C</api/v1/config/category/[UUID]>

=cut

sub category_get : Chained('category_instance_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{result} = $c->stash->{category};
}

=head2 category_list

=head2 URL Path

C</api/v1/config/category>

=cut

sub category_list : Chained('category_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{result} = Zaaksysteem::API::v1::ArraySet->new(
        is_paged => 0,
        content  => [$c->stash->{categories}->search(qb('config/category'))]
    );
}

=head2 definition_get

=head3 URL Path

C</api/v1/config/definition/[UUID]>

=cut

sub definition_get : Chained('definition_instance_base') : PathPart('')
    : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{result} = $c->stash->{definition};
}

=head2 definition_list

=head3 URL Path

C</api/v1/config/definition>

=cut

sub definition_list : Chained('definition_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{result} = Zaaksysteem::API::v1::ArraySet->new(
        is_paged => 0,
        content  => [$c->stash->{definitions}->search(qb('config/definition'))]
    );
}

=head2 item_create

=head3 URL Path

C</api/v1/config/item/create>

=cut

sub item_create : Chained('item_base') : PathPart('create') : Args(0) {
    my ($self, $c) = @_;

    $self->assert_post($c);
}

=head2 item_get

=head3 URL Path

C</api/v1/config/item/[UUID]>

=cut

sub item_get : Chained('item_instance_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{result} = $c->stash->{item};
}

=head2 item_list

=head3 URL Path

C</api/v1/config/item>

=cut

sub item_list : Chained('item_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{result} = Zaaksysteem::API::v1::ArraySet->new(
        is_paged => 0,
        content  => [$c->stash->{items}->search(qb('config/item'))]
    );
}

=head2 item_update

=head3 URL Path

C</api/v1/config/item/[UUID]/update>.

=cut

sub item_update : Chained('item_instance_base') : PathPart('update') : Args(0)
{
    my ($self, $c) = @_;

    $self->assert_post($c);
}

=head2 panel

=head2 URL Path

C</api/v1/config/panel>

=cut

sub panel_get : Chained('base') : PathPart('panel') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{result} = $c->stash->{model}->panel_instance;
}

=head2 update

Update configuration items in bulk.

The logged-in user needs admin rights for this to work.

=head2 URL Path

C</api/v1/config/update>

=cut

define_profile items_update => (required => { items => 'HashRef', },);

sub items_update : Chained('base') : PathPart('update') : Args(0) {
    my ($self, $c) = @_;

    $self->assert_post($c);
    $c->assert_any_user_permission('admin');

    my $args = assert_profile($c->req->params)->valid;

    my $model = $c->stash->{model};

    my @uuids = keys %{ $args->{items} };

    foreach (keys %{ $args->{items} }) {
        throw('item/update/uuid/invalid', sprintf('"%s" is not a UUID!', $_))
            unless UUID->check($_);
    }

    my $items = $model->search_items(@uuids);
    $items = $model->update_items($items, $args->{items});

    $c->stash->{result}
        = Zaaksysteem::API::v1::ArraySet->new(content => $items)
        ->init_paging($c->req);
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017-2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
