package Zaaksysteem::Controller::Man;

use Moose;

use File::Spec::Functions qw(catfile);

use Zaaksysteem::POD;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 index

Controller for zaaksysteem manual pages (POD to HTML)

=cut

sub index :Path :Args() {
    my ( $self, $c, $manual ) = @_;

    my $p           = Zaaksysteem::POD->new({
        homedir     => $c->config->{home},
        index       => 1,
    });

    my $index       = $p->index_list;
    $p->output_string(\$c->stash->{pod_output});

    # TODO:
    # Perhaps this logic should move to Zaaksysteem::POD
    my $file = $manual ? $index->{$manual} : undef ;

    if ($file) {
        $file = catfile($c->config->{home}, $index->{$manual});
        $p->requested_object($manual);
    }

    if (!$file) {
        $p->requested_object('Zaaksysteem::Manual');
        $file = catfile($c->config->{home}, qw(lib Zaaksysteem Manual.pod));
    }

    $p->load_html_header_after_title;
    $p->load_html_footer;

    $p->parse_file($file);

    $c->res->body($c->stash->{pod_output});
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
