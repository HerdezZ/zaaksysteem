package Zaaksysteem::ZTT::Converter::Pandoc::CLI;

=head1 NAME

Zaaksysteem::ZTT::Converter::Pandoc::CLI - Pandoc conversion for the commandline

=cut

use Moose::Role;

use List::Util 'pairs';

=head1 DESCRIPTION

A role that implements `convert`.

=cut

=head1 NOTE

Unfortunately, the CPAN moduyle L<Pandoc> has a bit odd interface, wich I could
not master. And this module I already had and provides 'perlish' options, rather
than the options offered through that module (that one was quit straight to the
commandline)

=cut

=head1 ATTRIBUTES

=head2 pandoc_path

The location of the executable. It will use <$ENV{PANDOC_PATH} by default and
fall back to C<pandoc>, relying on the operating system.

=cut

has pandoc_path => (
    is => 'rw',
    lazy => 1,
    builder => '_build_pandoc_path',
);

sub _build_pandoc_path {
    return $ENV{PANDOC_PATH} || 'pandoc'
}

=head1 METHODS

=head2 convert

Implements an 'around' method modifier.

See L<Zaaksysteem::ZTT::Converter::Pandoc> for what this method should expect.

=cut

around 'convert' => sub {
    my $orig = shift;
    my $self = shift;
    my %params = @_;

    my @system_call = _param_options_to_system_list( $self->pandoc_path,
        [   # arrayref, for the order of options might be important
            'from'          => $params{convert_from},
            'to'            => $params{convert_into},
            'reference-doc' => $params{reference_file},
#           'reference-odt' => # depricated since pandoc 2
            'output'        => $params{output_file},
        ],
        $params{input_file}
    );

    return _system(@system_call);
};

# turn a arrayref of key-value pairs into the @cmnd list for `system`
# pushing @cmnd, --$key, $value
# pushing the remaining other arguments passed in
sub _param_options_to_system_list {
    my $executable = shift;
    my $options    = shift;
    my @arguments  = @_;

    my @cmnd = $executable;
    foreach my $option ( pairs @$options ) {
        my $key = $option->key;
        my $value = $option->value;
        push (@cmnd, "--$key");
        push (@cmnd, $value) if defined $value;
    }
    push @cmnd, @arguments;

    return @cmnd
}

sub _system {
    return system ( @_ );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
