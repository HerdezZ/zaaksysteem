=head1 NAME

Zaaksysteem::Manual::API::V1::Types - Object type listing

=head1 DESCRIPTION

This page lists documentation for all object types used in the v1 API.

=head1 TYPES

=head2 Core types

=over 4

=item L<C<set>|Zaaksysteem::Manual::API::V1::Types::Set>

=item L<C<status>|Zaaksysteem::Manual::API::V1::Types::Status>

=item L<C<exception>|Zaaksysteem::Manual::API::V1::Types::Exception>

=back

=head2 Case types

=over 4

=item L<C<case>|Zaaksysteem::Manual::API::V1::Types::Case>

=item L<C<casetype>|Zaaksysteem::Manual::API::V1::Types::Casetype>

=item L<C<casetype_acl>|Zaaksysteem::Manual::API::V1::Types::CasetypeACL>

=back

=head2 System types

=over 4

=item L<C<sysin/remote_call_response>|Zaaksysteem::Manual::API::V1::Types::Sysin::RemoteCallResponse>

=back

=head2 Other types

=over 4

=item L<C<appointment>|Zaaksysteem::Manual::API::V1::Types::Appointment>

=back

=cut

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
