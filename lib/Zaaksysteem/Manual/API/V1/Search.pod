=head1 NAME

Zaaksysteem::Manual::API::V1::Search - Searching within specified components of zaaksysteem.nl

=head1 Description

This API-document describes the usage of our JSON Search API. Via the Search API it is possible
to search within different components.

=head1 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 search

   /api/v1/search/COMPONENT

Retrieving multiple search hits from zaaksysteem is as easy as calling the url C</api/v1/search> with the
parameters C<query> and optionally C<filter>.

B<Example call>

 https://localhost/api/v1/search/casetype?query=rrruaarr&filter=trigger:intern

B<Params>

=over 4

=item query [required]

B<TYPE>: Str

The query to search for, can be any kind of string, as long it's B<at least> 3 characters long

=item filter

B<TYPE>: Str

   api/v1/search/casetype?query=With&filter=trigger:intern&filter=subject_type:natuurlijk_persoon

   api/v1/search/casetype?query=With&filter=trigger:intern

=back

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-aca8c2-04f79a",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 1,
            "total_rows" : 1
         },
         "rows" : [
            {
               "instance" : {
                  "description" : "Dit is een omschrijving",
                  "label" : "With a rrruaarr title",
                  "reference" : "cc39a596-e6e7-4d76-bfc9-33d3eaca127c",
                  "score" : 1,
                  "search_type" : "casetype"
               },
               "reference" : null,
               "type" : "search"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 COMPONENTS (Search Types)

For now, it is not possible to ask "search" to query every component of zaaksysteem. We simply do not have
the resources for that. But it is possible to search on one component.

Below is a list of components and the allowed filters

=head2 casetype

Searches within all active casetypes of our zaaksysteem.nl, and returns the hits matching your criteria

B<Filters>

=over 4

=item trigger

   api/v1/search/casetype?query=With&filter=trigger:intern

The trigger for this casetype, could be "intern", "extern" and "internextern"

=item betrokkene_type

   api/v1/search/casetype?query=With&filter=betrokkene_type:natuurlijk_persoon

The betrokkene_type to search casetypes for. Now every casetype can be requested for a natuurlijk_persoon,
so if you would like to limit the casetype to this betrokkene_type, supply this filter.

=back

=head1 Objects

=head2 Search

Most of the calls in this document return an instance of type C<search>. Below we provide more information about
the contents of this object.

B<Properties>

=over 4

=item reference

B<TYPE>: UUID

The reference to this object. Together with C<search_type> you will be able to construct a URL to get
more detailed information about this object.

=item search_type

B<TYPE>: Str

A search_type, defining the base part of the API url to call when trying to retieve the full object.

=item score

B<TYPE>: Integer

The score of this hit, for now, it will always return a 1 (one)

=item label

B<TYPE>: Str

The label of this search hit.

=item description

A small description.

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Dashboard>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
