=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Status - Type definition for status
objects

=head1 DESCRIPTION

This page documents the serialization for
C<status|Zaaksysteem::API::v1::Status> objects.

=head1 JSON

=begin javascript

{
    "reference": null,
    "type": "status",
    "instance": {
        "version": "3.25.10"
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 version

Holds a version string for the used instance of Zaaksysteem.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
