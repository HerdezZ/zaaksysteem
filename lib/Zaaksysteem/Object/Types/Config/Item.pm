package Zaaksysteem::Object::Types::Config::Item;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Config::Item - Data wrapper for C<config/item>
objects

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Syzygy::Types qw[Name];

=head1 METHODS

=head2 type

Returns the object type string C<config/item>.

=cut

override type => sub { return 'config/item' };

=head1 ATTRIBUTES

=head2 name

Defines the name of the config item/definition.

=cut

has name => (
    is => 'ro',
    isa => Name,
    traits => [qw[OA]],
    label => 'Configuratie item naam'
);

=head2 value

Value of the configuration item.

=cut

has value => (
    is      => 'rw',
    traits  => [qw[OA]],
    label   => 'Configuratie item waarde',
    clearer => 'clear_value',
);

=head2 definition

A L<Zaaksysteem::Object::Types::Config::Definition> object or a reference to
it.

=cut

has definition => (
    is => 'rw',
    type => 'config/definition',
    label => 'Configuratie item definitie',
    traits => [qw[OR]],
    required => 1
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
