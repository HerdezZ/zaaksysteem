package Zaaksysteem::Search::Conditional::Keyword;

use Moose;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Search::Conditional::Keyword - Keyword matching search
conditional

=head1 ATTRIBUTES

=head2 value

Stores the keywords to match records on

=cut

has value => (
    is => 'ro',
    required => 1
);

=head1 METHODS

=head2 evaluate

Returns SQL that performs the match query.

=cut

sub evaluate {
    my ($self, $rs, $parent_cond) = @_;

    unless ($rs->can('search_text_vector')) {
        throw('search/keyword/not_supported', sprintf(
            'Keyword search not supported for "%s" resource',
            $rs->result_source->name
        ));
    }

    my $cond = $rs->build_text_vector_condition($self->value);

    return unless $cond;

    # Pretend we're SQL::Abstract for a bit... anyone know how to hoist this
    # transformation via DBIx::Class?
    my $tsquery = $cond->{ $rs->text_vector_column }{ '@@' };

    return \[
        sprintf('%s @@ ?', $rs->text_vector_column),
        [ {} => $tsquery ]
    ];
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
