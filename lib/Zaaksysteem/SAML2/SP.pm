package Zaaksysteem::SAML2::SP;

use Moose;

extends 'Net::SAML2::SP';
with 'MooseX::Log::Log4perl';

use BTTW::Tools;
use File::Temp;
use List::Util qw(any first);

use constant SIGNING_BLOCK => qq{
    <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
      <ds:SignedInfo>
        <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
        <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
        <ds:Reference URI="#_REPLACE_INTERFACE_UUID">
          <ds:Transforms>
            <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
            <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
          </ds:Transforms>
          <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
          <ds:DigestValue />
        </ds:Reference>
        </ds:SignedInfo>
      <ds:SignatureValue />
      <ds:KeyInfo>
        <ds:X509Data>
          <ds:X509Certificate>REPLACE_CERTIFICATE</ds:X509Certificate>
        </ds:X509Data>
        <ds:KeyName>REPLACE_KEYNAME</ds:KeyName>
      </ds:KeyInfo>
    </ds:Signature>
};

use constant MD_NS => ['md' => 'urn:oasis:names:tc:SAML:2.0:metadata'];
use constant DS_NS => ['ds' => 'http://www.w3.org/2000/09/xmldsig#'];

has id => ( is => 'rw', isa => 'Str', required => 1 );

has interface => ( is => 'rw', isa => 'Zaaksysteem::Model::DB::Interface' );

# When using a Swift storage backend, we do get a filehandle which we cannot allow
# to go out of scope during this session. We place cert_path in _cert_fh for persistence.
has _cert_fh => ( is => 'rw' );

has main_idp => (
    is        => 'rw',
    isa       => 'Zaaksysteem::Model::DB::Interface',
    required  => 0,
    predicate => 'has_main_idp',
);

has idps => (
    is => 'rw',
    isa => 'ArrayRef[Zaaksysteem::Model::DB::Interface]',
    required => 1,
);

=head1 SAML2::SP

This class wraps L<Net::SAML2::SP> for a bit nicer integration with
L<Zaaksysteem>.

=head1 Example usage

    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        $my_interface_object
    );

    my $metadata = $sp->metadata;

=head1 Methods

=head2 new_from_interface

This methods wraps the logic of building a SP object from an interface
definition. Works like the normal C<new> function, except that it requires
an interface object be supplied as the first argument. Remaining parameters
are interpreted as a hash passed directly to C<new>.

=head3 interface

An L<Zaaksysteem::Backend::Sysin::Interface::Component> instance.

=cut

define_profile new_from_interface => (
    required => {
        interface => 'Zaaksysteem::Model::DB::Interface',
    },
    optional => {
        idp         => 'Zaaksysteem::Model::DB::Interface',
        main_idp_id => 'Int',
    }
);

sub new_from_interface {
    my ($class, %params) = @_;

    my $valid = assert_profile(\%params)->valid;

    my $sp_interface = $valid->{ interface };
    my $sp_config = $sp_interface->get_interface_config;
    my $schema = $sp_interface->result_source->schema;

    my $idps;
    if ($valid->{idp} && ref($valid->{idp}) ne 'ARRAY') {
        $idps = [ $valid->{idp} ];
    } elsif(not defined $valid->{idp}) {
        $idps = [ ]
    } else {
        $idps = $valid->{idp};
    }

    my $cert;

    my $main_idp;
    if ($valid->{main_idp_id}) {
        $main_idp = first {
            $_->id == $valid->{main_idp_id};
        } @$idps;

        throw(
            'saml2/sp/metadata/main_idp_not_found',
            "Could not find IdP with id = '$valid->{main_idp_id}' to include in metadata"
        ) unless $main_idp;
    } elsif (@$idps == 1) {
        $main_idp = $idps->[0];
    }

    if ($main_idp && $main_idp->jpath('$.idp_cert[0].id')) {
        $cert = $schema->resultset('Filestore')->find($main_idp->jpath('$.idp_cert[0].id'));
    }
    else {
        $cert = $schema->resultset('Filestore')->find($sp_interface->jpath('$.sp_cert[0].id'));
    }

    unless($cert) {
        throw('saml2/sp/certificate', 'Certificate configured for SP not found in filestore.');
    }

    my $contact;
    if (my $contact_data = $sp_config->{contact_id}) {
        $contact = $schema->betrokkene_model->get(
            {type => $contact_data->{object_type}},
            $contact_data->{id},
        );
    }

    # When using a Swift storage backend, we do get a filehandle to a temporary file which
    # gets deleted when the handle goes out of scope.
    # We place $cert_path in _cert_fh so it doesn't go out of scope prematurely.
    my $cert_path       = $cert->get_path;

    my %new_args = (
        id               => $sp_config->{sp_webservice},
        url              => $sp_config->{sp_webservice},
        interface        => $sp_interface,
        cert             => ("$cert_path" || ''),       # Stringify to filename
        _cert_fh         => $cert_path,
        cacert           => '',
        org_name         => $sp_config->{sp_application_name},
        org_display_name => $sp_config->{sp_application_name},
        org_contact      => ($contact ? $contact->email : ''),
        idps             => $idps,
        defined $main_idp
            ? (main_idp => $main_idp)
            : (),
    );

    return $class->new( %new_args );
}

sub signed_metadata {
    my $self            = shift;
    my ($options)       = @_;

    my $xml             = $self->metadata(@_);

    ### Sign the XML
    if (-e '/usr/bin/xmlsec1') {
        my $privfile        = $self->cert;
        my $certtext        = $self->_cert_text;

        my $md5cert         = Digest::MD5::md5_hex( $certtext );
        my $interface_uuid  = $self->interface->uuid;

        my $replace         = "\n" . SIGNING_BLOCK;
        $replace            =~ s/REPLACE_KEYNAME/$md5cert/;
        $replace            =~ s/REPLACE_CERTIFICATE/$certtext/;
        $replace            =~ s/REPLACE_INTERFACE_UUID/$interface_uuid/;

        $xml                =~ s/(<md:EntityDescriptor(?:[^>]*)>)/$1$replace/sg;

        my $tmph = File::Temp->new(
            UNLINK => 1,
            SUFFIX => '.xml'
        );

        my $tmpfile = $tmph->filename;

        open(my $FH, '>:encoding(UTF-8)', $tmpfile) or throw(
            '/auth/saml/sign_error',
            "Failed witing metadata XML for signing: '$!'"
        );

        print $FH $xml;
        close($FH);

        if ($privfile && $tmpfile) {
            open(
                my $fh,
                '-|',
                qw(
                    /usr/bin/xmlsec1
                    --sign
                    --id-attr:ID urn:oasis:names:tc:SAML:2.0:metadata:EntityDescriptor
                    --privkey-pem
                ),
                $privfile,
                $tmpfile
            );

            binmode($fh);
            $xml = join("", <$fh>);
            close $fh;
        }
    }

    return $xml;
}

sub _get_sso_bindings {
    my $self = shift;
    my $x = shift;

    my @sso = (
        $x->SingleLogoutService(
            MD_NS,
            { Binding => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
              Location  => $self->url . '/saml/sls-redirect' },
        ),
    );
    return @sso unless $self->has_main_idp;

    my $config = $self->main_idp->get_interface_config;
    if ($config->{saml_type} eq 'digid' && $config->{use_saml_slo}) {

        push(@sso, $x->SingleLogoutService(
            MD_NS,
            {
                Binding =>
                    'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
                Location => $self->url . '/saml/sls-soap'
            },
        ));
    }
    return @sso;
}

sub metadata {
    my ($self, $options) = @_;

    ### START COPY FROM Net::SAML2::SP
    my $x = XML::Generator->new(':pretty', conformance => 'loose');

    my $entity_id;

    ### Hack for eHerkenning
    if ($options && $options->{entity_id}) {
        $entity_id = $options->{entity_id};
    }
    elsif ($self->has_main_idp) {
        my $config = $self->main_idp->get_interface_config;

        $entity_id = $config->{idp_entity_id};

        unless (defined($entity_id) && length($entity_id)) {
            $self->log->trace(
                "No idp_entity_id set in IdP; falling back to SP id");
            $entity_id = $self->id;
        }
    }
    else {
        $entity_id = $self->id;
    }


    my $metadataobject = $x->EntityDescriptor(
        [ @{ MD_NS() }, @{ DS_NS() } ],
        {
            'entityID'    => $entity_id,
            'ID'          => sprintf('_%s', $self->interface->uuid),
        },
        $x->SPSSODescriptor(
            MD_NS,
            { AuthnRequestsSigned => 'true',
              WantAssertionsSigned => 'true',
              errorURL => $self->url . '/saml/error',
              protocolSupportEnumeration => 'urn:oasis:names:tc:SAML:2.0:protocol', },
            $self->_generate_key_descriptors($x, $options),

            $self->_get_sso_bindings($x),

            $x->AssertionConsumerService(
                MD_NS,
                { Binding => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                  Location => $self->url . '/saml/consumer-post',
                  index => '1',
                  isDefault => 'true' },
            ),
            $x->AssertionConsumerService(
                MD_NS,
                { Binding => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
                  Location => $self->url . '/saml/consumer-artifact',
                  index => '2',
                  isDefault => 'false' },
            ),
            $self->_generate_attribute_consuming_services($x, $options),
        ),
        $x->Organization(
            MD_NS,
            $x->OrganizationName(
                MD_NS,
                { 'xml:lang' => 'en' },
                $self->org_name,
            ),
            $x->OrganizationDisplayName(
                MD_NS,
                { 'xml:lang' => 'en' },
                $self->org_display_name,
            ),
            $x->OrganizationURL(
                MD_NS,
                { 'xml:lang' => 'en' },
                $self->url
            )
        ),
        $x->ContactPerson(
            MD_NS,
            {
                contactType => 'other' },
            $x->Company(
                MD_NS,
                $self->org_display_name,
            ),
            $x->EmailAddress(
                MD_NS,
                $self->org_contact,
            ),
        )
    );
    ### END COPY FROM Net::SAML2::SP


    my $xml             = "$metadataobject";

    ### Poor mans matching, replace the extra /saml/, e.g.:
    ### replace http://zaaksysteem.nl/auth/saml/saml/consumer-post
    ### with http://zaaksysteem.nl/auth/saml/consumer-post
    $xml                =~ s|/saml/saml/|/saml/|g;

    return $xml;
}

=head2 cert_name

Generate the certificate name for SAML metadata (and decryption).

For now, this is the MD5 of the certificate file.

=cut

sub cert_name {
    my $self = shift;
    return Digest::MD5::md5_hex( $self->_cert_text );
}

sub _generate_key_descriptors {
    my $self = shift;
    my $x = shift;
    my $options = shift;

    my $md5cert = $self->cert_name;

    my @rv = $x->KeyDescriptor(
        MD_NS,
        { use => 'signing' },
        $x->KeyInfo(
            DS_NS,
            $x->X509Data(
                DS_NS,
                $x->X509Certificate(
                    DS_NS,
                    $self->_cert_text,
                ),
            ),
            $x->KeyName(
                DS_NS,
                $md5cert
            )
        ),
    );

    if (any { $_->get_interface_config->{saml_type} eq 'eidas' } @{ $self->idps }) {
        push @rv, $x->KeyDescriptor(
            MD_NS,
            { use => 'encryption' },
            $x->KeyInfo(
                DS_NS,
                $x->X509Data(
                    DS_NS,
                    $x->X509Certificate(
                        DS_NS,
                        $self->_cert_text,
                    ),
                ),
                $x->KeyName(
                    DS_NS,
                    $md5cert
                )
            ),
        );
    }

    return @rv;
}

sub _generate_attribute_consuming_services {
    my $self    = shift;
    my $x       = shift;
    my $options = shift;

    my @acs;
    for my $idp (@{ $self->idps }) {
        my $config = $idp->get_interface_config;

        # Currently, we only request attributes in eIDAS metadata.
        # ADFS does not support this (yet), and requires manual configuration.
        if ($config->{saml_type} eq 'eidas') {
            push @acs, $x->AttributeConsumingService(
                MD_NS,
                {
                    isDefault => "true",
                    index => $config->{idp_acs_index},
                },
                $x->ServiceName(
                    MD_NS,
                    $self->org_display_name,
                ),
                $x->RequestedAttribute(
                    MD_NS,
                    {
                        Name => $config->{idp_service_id}
                    },
                ),
                $x->RequestedAttribute(
                    MD_NS,
                    {
                        isRequired => "false",
                        PurposeStatement => "Desired for service",
                        Name => "urn:etoegang:1.9:attribute:FirstName"
                    },
                ),
                $x->RequestedAttribute(
                    MD_NS,
                    {
                        isRequired => "false",
                        PurposeStatement => "Desired for service",
                        Name => "urn:etoegang:1.9:attribute:FamilyNameInfix"
                    },
                ),
                $x->RequestedAttribute(
                    MD_NS,
                    {
                        isRequired => "false",
                        PurposeStatement => "Desired for service",
                        Name => "urn:etoegang:1.9:attribute:FamilyName"
                    },
                ),
                $x->RequestedAttribute(
                    MD_NS,
                    {
                        isRequired => "false",
                        PurposeStatement => "Desired for service",
                        Name => "urn:etoegang:1.9:attribute:DateOfBirth"
                    },
                ),
            );
        }
    }

    return @acs;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 SIGNING_BLOCK

TODO: Fix the POD

=cut

=head2 metadata

TODO: Fix the POD

=cut

=head2 signed_metadata

TODO: Fix the POD

=cut

