package Zaaksysteem::SAML2::Protocol::LogoutRequest;
use Moose;

extends 'Net::SAML2::Protocol::LogoutRequest';
with 'Zaaksysteem::SAML2::Protocol::Role::RandomID';

has '+nameid_format' => ( required => 0);

use MooseX::Types::Moose qw/ Str /;
use XML::Generator;

=head1 NAME

Zaaksysteem::SAML2::Protocol::LogoutRequest

=head1 DESCRIPTION

This module wraps L<Net::SAML2::Protocol::LogoutRequest> and outputs XML that
AD FS can understand.

=head1 Example usage

Constructor is called in default L<Moose> style.

    my $logoutreq = Zaaksysteem::SAML2::Protocol::LogoutRequest->new(
        issuer        => 'sp_entity_id',
        destination   => $idp->slo_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),

        # These 3 should match what the IdP returned on login success:
        nameid        => 'xxxxx',
        nameid_format => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
        session       => 'session_id',
    );

    my $str = $logoutreq->as_xml;

=head1 Attributes

=head1 Inherited attributes

See L<Net::SAML2::Protocol::LogoutRequest>

=cut

=head1 Methods

=head2 as_xml

Returns a L<XML::Generator::final> object representing the
C<LogoutRequest> XML message.

=cut

sub as_xml {
    my ($self) = @_;

    my $x = XML::Generator->new(':pretty');
    my $saml  = ['saml' => 'urn:oasis:names:tc:SAML:2.0:assertion'];
    my $samlp = ['samlp' => 'urn:oasis:names:tc:SAML:2.0:protocol'];

    my %format = ();
    if (defined $self->nameid_format) {
        $format{Format} = $self->nameid_format;
    }

    $x->xml(
        $x->LogoutRequest(
            $samlp,
            { ID => $self->random_id,
              IssueInstant => $self->issue_instant,
              Version => '2.0',
              Destination => $self->destination },
            $x->Issuer(
                $saml,
                $self->issuer,
            ),
            $x->NameID(
                $saml, \%format,
                $self->nameid,
            ),
            $x->SessionIndex(
                $samlp,
                $self->session,
            ),
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
