package Zaaksysteem::API::v1::Serializer::Reader::Sysin;

use Moose;

with qw[
    Zaaksysteem::API::v1::Serializer::DispatchReaderRole
];

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Sysin

=head1 DESCRIPTION

Serializer for Zaaksysteem Sysin instances

=cut

use BTTW::Tools;

=head1 METHODS

=head2 dispatch_map

Implements serializer dispatcher for
L<Zaaksysteem::API::v1::Serializer::DispatchReaderRole/dispatch_map>.

=cut

sub dispatch_map {
    return (
        'Zaaksysteem::Backend::Sysin::Transaction::Component' => sub {
            return __PACKAGE__->read_transaction_object(@_);
        },

        'Zaaksysteem::Backend::Sysin::TransactionRecord::Component' => sub {
            return __PACKAGE__->read_transaction_record_object(@_);
        }
    );
}

=head2 read_transaction_object

Reader for L<Zaaksysteem::Backend::Sysin::Transaction::Component> instances.

=cut

sub read_transaction_object {
    my ($class, $serializer, $txn) = @_;

    # Undesired interface deref, it doesn't matter in the case of the
    # api/v1/sysin/interface/transaction codepaths, but buyer beware: this
    # serializer may trigger database fetches.

    return {
        type => 'sysin/transaction',
        reference => $txn->uuid,
        instance => {
            direction => $txn->direction,
            date_created => $serializer->read($txn->date_created),
            date_deleted => $serializer->read($txn->date_deleted),
            date_next_retry => $serializer->read($txn->date_next_retry),
            date_last_retry => $serializer->read($txn->date_last_retry),

            records => {
                type => 'set',
                reference => undef,
                preview => 'Collection of sysin/transaction/record objects',
                instance => {
                    rows => [
                        map {
                            $class->read_transaction_record_object($serializer, $_)
                        } $txn->transaction_records->all
                    ]
                }
            },

            interface => {
                type => 'interface',
                instance => undef,
                reference => $txn->interface_id->uuid,
                preview => $txn->interface_id->name,
            },
        }
    };
}

=head2 read_transaction_record_object

Reader for L<Zaaksysteem::Backend::Sysin::TransactionRecord::Component>
instances.

=cut

sub read_transaction_record_object {
    my ($class, $serializer, $txn_record) = @_;

    return {
        type => 'sysin/transaction/record',
        reference => $txn_record->uuid,
        preview => $txn_record->preview_string,
        instance => {
            input => $txn_record->input,
            output => $txn_record->output,
            is_error => $txn_record->is_error ? \1 : \0,
            date_executed => $serializer->read($txn_record->date_executed),
            preview_string => $txn_record->preview_string,
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
