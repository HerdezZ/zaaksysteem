package Zaaksysteem::API::v1::Serializer::Reader::Config;
use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Config - Reader for "Config" database rows

=head1 SYNOPSIS

=head1 METHODS

=head2 class

Returns the supported class ("Zaaksysteem::Schema::Config") for serialization.

=cut

sub class { 'Zaaksysteem::Schema::Config' }

=head2 read

Turn a config row into an APIv1 result

=cut

sub read {
    my ($class, $serializer, $config) = @_;

    my $value = $config->value;
    if ($config->parameter eq 'case_distributor_group') {
        $value = $class->_inflate_group($config);
    } elsif ($config->parameter eq 'case_distributor_role') {
        $value = $class->_inflate_role($config);
    }

    return {
        reference => $config->parameter,
        instance => {
            parameter => $config->parameter,
            value     => (blessed($value) ? $serializer->read($value) : $value),
        },
        type      => 'config',
    };
}

=head2 _inflate_role

    $class->_inflate_role(10002);

Turns a simple id in a L<Zaaksysteem::Object::Types::Role> object.

=cut

sub _inflate_role {
    my ($class, $config) = @_;

    my $value = $config->value;
    return unless int($value);

    my $role = $config->result_source->schema->resultset('Roles')->find($value);

    return unless $role;

    return Zaaksysteem::Object::Types::Role->new(
        role_id     => $role->id,
        name        => $role->name,
        description => ($role->description || $role->name),
        system_role => $role->system_role,
    );
}

=head2 _inflate_group

    $class->_inflate_group(20002);

Turns a simple id in a L<Zaaksysteem::Object::Types::Group> object

=cut

sub _inflate_group {
    my ($class, $config) = @_;

    my $value = $config->value;
    return unless int($value);

    my $group = $config->result_source->schema->resultset('Groups')->find($value);

    return unless $group;

    return Zaaksysteem::Object::Types::Group->new(
        group_id    => $group->id,
        name        => $group->name,
        description => ($group->description || $group->name),
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
