package Zaaksysteem::Schema::CaseACL;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');
__PACKAGE__->table('case_acl');

__PACKAGE__->result_source_instance->is_virtual(0);

__PACKAGE__->add_columns(
    'case_id'      => { data_type => 'integer' },
    'case_uuid'    => { data_type => 'UUID' },
    'subject_id'   => { data_type => 'integer' },
    'subject_uuid' => { data_type => 'UUID' },
    'casetype_id'  => { data_type => 'integer' },
    'permission'   => { data_type => 'text' },
);

__PACKAGE__->belongs_to(
    "case_id",
    "Zaaksysteem::Schema::Zaak",
    {
        'foreign.id'          => 'self.case_id',
        'foreign.zaaktype_id' => 'self.casetype_id',
    }
);

__PACKAGE__->belongs_to(
    "case_uuid",
    "Zaaksysteem::Schema::Zaak",
    {
        'foreign.uuid' => 'self.case_uuid',
        'foreign.zaaktype_id' => 'self.casetype_id',
    }
);

__PACKAGE__->belongs_to(
    "subject_id",
    "Zaaksysteem::Schema::Subject",
    {
        'foreign.id' => 'self.subject_id',
    }
);

__PACKAGE__->belongs_to(
    "subject_uuid",
    "Zaaksysteem::Schema::Subject",
    {
        'foreign.uuid' => 'self.subject_uuid',
    }
);

__PACKAGE__->belongs_to(
    "casetype_id",
    "Zaaksysteem::Schema::Zaaktype",
    {
        'foreign.id' => 'self.casetype_id',
    }
);

1;

__END__

=head1 NAME

Zaaksysteem::Schema::CaseACL - A view for case ACL's

=head1 DESCRIPTION

A result source like object for the C<case_acl> table which is found in
PostgreSQL.

=head1 SYNOPSIS

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
