use utf8;
package Zaaksysteem::Schema::ThreadMessageExternal;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ThreadMessageExternal

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<thread_message_external>

=cut

__PACKAGE__->table("thread_message_external");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'thread_message_external_id_seq'

=head2 type

  data_type: 'text'
  is_nullable: 0

=head2 content

  data_type: 'text'
  is_nullable: 0

=head2 subject

  data_type: 'text'
  is_nullable: 0

=head2 participants

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 direction

  data_type: 'text'
  is_nullable: 0

=head2 source_file_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 read_pip

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 read_employee

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 attachment_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "thread_message_external_id_seq",
  },
  "type",
  { data_type => "text", is_nullable => 0 },
  "content",
  { data_type => "text", is_nullable => 0 },
  "subject",
  { data_type => "text", is_nullable => 0 },
  "participants",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "direction",
  { data_type => "text", is_nullable => 0 },
  "source_file_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "read_pip",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "read_employee",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "attachment_count",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 source_file_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "source_file_id",
  "Zaaksysteem::Schema::Filestore",
  { id => "source_file_id" },
);

=head2 thread_messages

Type: has_many

Related object: L<Zaaksysteem::Schema::ThreadMessage>

=cut

__PACKAGE__->has_many(
  "thread_messages",
  "Zaaksysteem::Schema::ThreadMessage",
  { "foreign.thread_message_external_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-02-11 12:09:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gPryr7QRFMAmHuFClV/9bA

use JSON::XS ();

__PACKAGE__->inflate_column('participants', {
    inflate => sub { JSON::XS->new->decode(shift // '[]') },
    deflate => sub { JSON::XS->new->canonical->encode(shift // []) },
});

1;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
