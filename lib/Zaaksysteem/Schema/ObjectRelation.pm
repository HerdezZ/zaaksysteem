use utf8;
package Zaaksysteem::Schema::ObjectRelation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ObjectRelation

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<object_relation>

=cut

__PACKAGE__->table("object_relation");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 object_type

  data_type: 'text'
  is_nullable: 0

=head2 object_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 object_embedding

  data_type: 'text'
  is_nullable: 1

=head2 object_id

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 object_preview

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "object_type",
  { data_type => "text", is_nullable => 0 },
  "object_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "object_embedding",
  { data_type => "text", is_nullable => 1 },
  "object_id",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "object_preview",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 object_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object_id",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object_id" },
);

=head2 object_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object_uuid" },
);


# Created by DBIx::Class::Schema::Loader v0.07048 @ 2018-02-28 10:06:42
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gbjwYmK6xBawWcBneZE+Iw

use JSON::XS qw();

__PACKAGE__->inflate_column('object_embedding', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub {
        no warnings 'redefine';
        local *DateTime::TO_JSON = sub { shift->iso8601 };

        JSON::XS->new->convert_blessed->encode(shift // {});
    },
});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
