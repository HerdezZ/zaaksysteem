use utf8;
package Zaaksysteem::Schema::ObjectData;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ObjectData

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<object_data>

=cut

__PACKAGE__->table("object_data");

=head1 ACCESSORS

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 object_id

  data_type: 'integer'
  is_nullable: 1

=head2 object_class

  data_type: 'text'
  is_nullable: 0

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 index_hstore

  data_type: 'hstore'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 date_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 text_vector

  data_type: 'tsvector'
  is_nullable: 1

=head2 class_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 acl_groupname

  data_type: 'text'
  is_nullable: 1

=head2 invalid

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "object_id",
  { data_type => "integer", is_nullable => 1 },
  "object_class",
  { data_type => "text", is_nullable => 0 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "index_hstore",
  { data_type => "hstore", is_nullable => 1 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "date_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "text_vector",
  { data_type => "tsvector", is_nullable => 1 },
  "class_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "acl_groupname",
  { data_type => "text", is_nullable => 1 },
  "invalid",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->set_primary_key("uuid");

=head1 UNIQUE CONSTRAINTS

=head2 C<object_data_object_class_object_id_key>

=over 4

=item * L</object_class>

=item * L</object_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "object_data_object_class_object_id_key",
  ["object_class", "object_id"],
);

=head1 RELATIONS

=head2 case_properties

Type: has_many

Related object: L<Zaaksysteem::Schema::CaseProperty>

=cut

__PACKAGE__->has_many(
  "case_properties",
  "Zaaksysteem::Schema::CaseProperty",
  { "foreign.object_id" => "self.uuid" },
  undef,
);

=head2 class_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "class_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "class_uuid" },
);

=head2 interfaces

Type: has_many

Related object: L<Zaaksysteem::Schema::Interface>

=cut

__PACKAGE__->has_many(
  "interfaces",
  "Zaaksysteem::Schema::Interface",
  { "foreign.objecttype_id" => "self.uuid" },
  undef,
);

=head2 loggings

Type: has_many

Related object: L<Zaaksysteem::Schema::Logging>

=cut

__PACKAGE__->has_many(
  "loggings",
  "Zaaksysteem::Schema::Logging",
  { "foreign.object_uuid" => "self.uuid" },
  undef,
);

=head2 object_acl_entries

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectAclEntry>

=cut

__PACKAGE__->has_many(
  "object_acl_entries",
  "Zaaksysteem::Schema::ObjectAclEntry",
  { "foreign.object_uuid" => "self.uuid" },
  undef,
);

=head2 object_bibliotheek_entries

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectBibliotheekEntry>

=cut

__PACKAGE__->has_many(
  "object_bibliotheek_entries",
  "Zaaksysteem::Schema::ObjectBibliotheekEntry",
  { "foreign.object_uuid" => "self.uuid" },
  undef,
);

=head2 object_datas

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->has_many(
  "object_datas",
  "Zaaksysteem::Schema::ObjectData",
  { "foreign.class_uuid" => "self.uuid" },
  undef,
);

=head2 object_mutation_lock_object_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectMutation>

=cut

__PACKAGE__->has_many(
  "object_mutation_lock_object_uuids",
  "Zaaksysteem::Schema::ObjectMutation",
  { "foreign.lock_object_uuid" => "self.uuid" },
  undef,
);

=head2 object_mutation_object_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectMutation>

=cut

__PACKAGE__->has_many(
  "object_mutation_object_uuids",
  "Zaaksysteem::Schema::ObjectMutation",
  { "foreign.object_uuid" => "self.uuid" },
  undef,
);

=head2 object_relation_object_ids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectRelation>

=cut

__PACKAGE__->has_many(
  "object_relation_object_ids",
  "Zaaksysteem::Schema::ObjectRelation",
  { "foreign.object_id" => "self.uuid" },
  undef,
);

=head2 object_relation_object_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectRelation>

=cut

__PACKAGE__->has_many(
  "object_relation_object_uuids",
  "Zaaksysteem::Schema::ObjectRelation",
  { "foreign.object_uuid" => "self.uuid" },
  undef,
);

=head2 object_relationships_object1_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectRelationships>

=cut

__PACKAGE__->has_many(
  "object_relationships_object1_uuids",
  "Zaaksysteem::Schema::ObjectRelationships",
  { "foreign.object1_uuid" => "self.uuid" },
  undef,
);

=head2 object_relationships_object2_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectRelationships>

=cut

__PACKAGE__->has_many(
  "object_relationships_object2_uuids",
  "Zaaksysteem::Schema::ObjectRelationships",
  { "foreign.object2_uuid" => "self.uuid" },
  undef,
);

=head2 queues

Type: has_many

Related object: L<Zaaksysteem::Schema::Queue>

=cut

__PACKAGE__->has_many(
  "queues",
  "Zaaksysteem::Schema::Queue",
  { "foreign.object_id" => "self.uuid" },
  undef,
);

=head2 zaaks

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaaks",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.uuid" => "self.uuid" },
  undef,
);

=head2 zaaktype_kenmerkens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeKenmerken>

=cut

__PACKAGE__->has_many(
  "zaaktype_kenmerkens",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { "foreign.object_id" => "self.uuid" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-12-20 10:07:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:T2qb3BD/a8gUhu0hPs/upg

use JSON qw/to_json from_json/;
use Zaaksysteem::JSON::Serializer;
use Zaaksysteem::DB::HStore;

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Object::Data::ResultSet');

__PACKAGE__->load_components(
    "+Zaaksysteem::Backend::Object::Data::Component",
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('date_modified',
    { %{ __PACKAGE__->column_info('date_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('date_created',
    { %{ __PACKAGE__->column_info('date_created') },
    set_on_create => 1,
});

__PACKAGE__->inflate_column('properties', {
    inflate => sub {
        Zaaksysteem::JSON::Serializer->from_json(shift);
    },
    deflate => sub {
        Zaaksysteem::JSON::Serializer->to_json(shift);
    }
});

__PACKAGE__->inflate_column('index_hstore', {
    inflate => sub {
        Zaaksysteem::DB::HStore::decode(shift);
    },
    deflate => sub {
        Zaaksysteem::DB::HStore::encode(shift);
    }
});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

