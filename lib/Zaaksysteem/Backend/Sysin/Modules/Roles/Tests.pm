package Zaaksysteem::Backend::Sysin::Modules::Roles::Tests;
use Moose::Role;

use BTTW::Tools;

use Array::Compare;
use Crypt::OpenSSL::X509;
use File::Temp;
use IO::All;
use IO::Socket::INET;
use IO::Socket::SSL qw(inet4);
use Moose::Util::TypeConstraints qw[union class_type];
use URI;
use BTTW::Tools::Tests qw(:all);

class_type('File::Temp');

# 365 days * 24 hours * 60 minutes * 60 seconds
my $CLIENT_CERT_MAX_AGE = 365 * 24 * 60 * 60;

# 180 days * 24 hours * 60 minutes * 60 seconds
my $CA_CERT_MAX_AGE = 180 * 24 * 60 * 60;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::Tests - Add simple server
connection tests capabilities to a sysin module

=head1 SYNOPSIS

    package My::Fancy::Module

    extends 'Zaaksysteem::Backend::Sysin::Modules';

    with 'Zaaksysteem::Backend::Sysin::Modules::Roles::Tests';

    ...

    sub my_test_hook {
        my $self = shift;

        my $socket = $self->test_host_port('some://url/to/some?resource');
    }

    # This test hook runs a HTTPS verification check
    sub my_ssl_test_hook {
        my $self = shift;

        my $socket = $self->test_host_port_ssl('https://someweb.site/path/is?ignored', '/path/to/ca_cert');
    }

=head1 METHODS

=head2 test_host_port

Connection testing. Will get the host and port from the given URL, and tries a simple
connect on the host and port.

    my $socket = $self->test_host_port('some://uyl/to/some?resource');

The L<URI> package will be used to attempt to parse the provided URL for it's
host and port components. Any other URI parts will be ignored.

Returns the L<IO::Socket::INET> instance if a connection is succesful, throws
an exception of type C<sysin/modules/test/error> if the test failed.

=cut

sub test_host_port {
    my $self = shift;
    my $endpoint = shift;

    my $uri = URI->new($endpoint);

    unless (defined $uri && $uri->isa('URI::http')) {
        throw('sysin/modules/test/endpoint_not_valid', sprintf(
            'Opgegeven URL "%s" is geen geldig endpoint',
            $endpoint
        ));
    }

    my $error = test_plain_connect($uri->host, $uri->port);

    if ($error) {
        throw('sysin/modules/test/error', sprintf(
            "Kon geen verbinding met externe server '%s' starten, staat de firewall inkomend verkeer toe en is de URL correct?\n\nInterne foutmelding: '%s'",
            $endpoint,
            $!,
        ));
    }

    return;
}

=head2 test_host_port_ssl

Connection testing. Will get the host and port from the given url, tries a
simple connect on the host and port and tries to do a TLS handshake, checking
if the server's certificate is signed by the supplied CA certificate chain.

    my $socket = $self->test_host_port_ssl('https://host.tld/path', '/path/to/ca_file');

The L<URI> package will be used to attempt to parse the provided URL for it's
scheme, host, and port components. Any other URL parts will be ignored.

Returns the L<IO::Socket::SSL> instance if the host is connectable and SSL
verification succeeded. If any part of the validation fails, a
C<sysin/modules/test/error> exception will be thrown.

The second parameter (path to the CA file to be used) is optional. If not
provided, the server's base CA store will be used.

=cut

sub test_host_port_ssl {
    my $self = shift;
    my ($endpoint, $ca_cert, $client_cert, $client_key) = @_;

    my $uri = URI->new($endpoint);

    unless (defined $uri && $uri->isa('URI::http')) {
        throw('sysin/modules/test/endpoint_not_valid', sprintf(
            'Opgegeven URL "%s" is geen geldig endpoint',
            $endpoint
        ));
    }

    unless ($uri->scheme eq 'https') {
        throw('sysin/modules/test/endpoint_not_https', sprintf(
            'Opgegeven URL "%s" is geen SSL-endpoint (controleer dat de endpoint begint met "https://")',
            $endpoint
        ));
    }

    my $error = test_secure_connect($uri->host, $uri->port, $ca_cert, $client_key, $client_cert);

    if ($error) {
        throw(
            'sysin/modules/test/error',
            sprintf(
                "Kon geen beveiligde verbinding maken met externe server '%s'.\n" .
                "Mogelijke oorzaken: de ingestelde URL is onjuist, een firewall staat ".
                "de verbinding niet toe, of een van de opgegeven certificaten of keys is ".
                "onjuist.\n\nInterne foutmelding: '%s'",
                $endpoint,
                $error,
            ),
            [ { ssl_error => $error } ]
        );
    }

    return;
}

=head2 test_ca_chain

Do some basic checks on the CA certificate chain, to see if it's complete.

=cut

sig test_ca_chain => union(['Str', 'File::Temp']);

sub test_ca_chain {
    my $self = shift;
    my $ca_file = shift;

    my $contents = io->file($ca_file)->slurp;

    my (@subjects, @issuers);
    my @messages;
    while ($contents =~ /(-----BEGIN CERTIFICATE-----.*?-----END CERTIFICATE-----)/gsm) {
        my $cert = Crypt::OpenSSL::X509->new_from_string($1, Crypt::OpenSSL::X509::FORMAT_PEM);
        $self->log->trace(sprintf("Found a client certificate. Subject = '%s'", $cert->subject));

        if ($cert->checkend(0)) {
            push @messages, sprintf(
                "Certificaat '%s' is verlopen.\n",
                $cert->subject,
            );
        }
        elsif ($cert->checkend($CA_CERT_MAX_AGE)) {
            push @messages, sprintf(
                "Certificaat '%s' zal binnen %d dagen verlopen.\n",
                $cert->subject,
                $CA_CERT_MAX_AGE / (24 * 60 * 60),
            );
        }

        push @subjects, $cert->subject;
        push @issuers, $cert->issuer;
    }


    if (   !@issuers
        || ($subjects[-1] ne $issuers[-1])
    ) {
        push @messages, "Certificaatketen eindigt niet met (self-signed) root-certificaat.\n";
    }
    else {
        # Pop the "self" issuer from that last certificate
        pop @issuers;
    }

    # Get the first CA certificate off the "subjects" list for easier comparing.
    my $base = shift @subjects;

    my $comp = Array::Compare->new();
    if (!$comp->compare( \@subjects, \@issuers ) || @messages) {
        throw(
            'sysin/modules/test/error',
            sprintf(
                  "Certificaatketen is onjuist of onvolledig.\n"
                . "%s\n"
                . "Geleverde certificaten:\n"
                . " %s\n"
                . "\n"
                . "Verwachte certificaten:\n"
                . " %s\n",
                join("\n", @messages),
                join("\n ", map { qq{"$_"} } $base, @subjects),
                join("\n ", map { qq{"$_"} } $base, @issuers),
            )
        );
    }

    return 1;
}

=head2 test_client_certificate

Do some basic checks on a client certificate file, to see if it's usable.

=cut

sig test_client_certificate => union(['Str', 'File::Temp']);

sub test_client_certificate {
    my $self = shift;
    my $combined = shift;

    my $contents = io->file($combined)->slurp;

    throw(
        'sysin/modules/test/error',
        "Client-certificaatbestand is onvolledig: bestand bevat geen private key"
    ) unless ($contents =~ /(-----BEGIN (RSA )?PRIVATE KEY-----.*?-----END (RSA )?PRIVATE KEY-----)/sm);

    throw(
        'sysin/modules/test/error',
        "Client-certificaatbestand is onvolledig: bestand bevat geen certificaat"
    ) unless ($contents =~ /^(-----BEGIN CERTIFICATE-----.*?-----END CERTIFICATE-----)$/sm);

    my $cert = Crypt::OpenSSL::X509->new_from_string($1, Crypt::OpenSSL::X509::FORMAT_PEM);

    my $cert_modulus = do {
        open my $openssl, "-|", "openssl", "x509", "-in", $combined, "-noout", "-modulus";
        <$openssl>;
    };
    my $key_modulus = do {
        open my $openssl, "-|", "openssl", "rsa", "-in", $combined, "-noout", "-modulus";
        <$openssl>;
    };

    if ($cert_modulus ne $key_modulus) {
        throw(
            'sysin/modules/test/error',
            "Client-certificaat en private key horen niet bij elkaar.",
        );
    }

    if ($cert->checkend(0)) {
        throw(
            'sysin/modules/test/error',
            "Client-certificaat is verlopen.",
        );
    }
    elsif ($cert->checkend($CLIENT_CERT_MAX_AGE)) {
        throw(
            'sysin/modules/test/error',
            sprintf(
                "Client-certificaat zal binnen %d dagen verlopen.",
                $CLIENT_CERT_MAX_AGE / (24 * 60 * 60)
            )
        );
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
