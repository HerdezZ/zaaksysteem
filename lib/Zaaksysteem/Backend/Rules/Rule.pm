package Zaaksysteem::Backend::Rules::Rule;

use Moose;
use BTTW::Tools;
use Moose::Util::TypeConstraints;
use Module::Load;

with 'MooseX::Log::Log4perl';

use Zaaksysteem::Backend::Rules::Rule::Condition;

use constant ACTION_TYPES    => {
    'set_value'             => 'SetValue',
    'set_value_formula'     => 'SetValueFormula',
    'set_value_magic_string'=> 'SetValueMagicString',
    'show_attribute'        => 'ShowHide',
    'hide_attribute'        => 'ShowHide',
    'show_text_block'       => 'ShowHideTextblock',
    'hide_text_block'       => 'ShowHideTextblock',
    'pause_application'     => 'Pause',
    'hide_group'            => 'HideGroup',
    'show_group'            => 'HideGroup',
    'set_allocation'        => 'SetAllocation',
};

for (values %{ACTION_TYPES()}) {
    load  __PACKAGE__ . "::Action::$_";
}


=head1 NAME

Zaaksysteem::Backend::Rules::Rule - Single rule within zaaksysteem

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem Rules engine, in charge of generating decision trees, and manipulating
contexts (like a case) according to anwers on these decissions.

For now, this object has support for generating a decision tree which can be used
with our mintjs frontend framework.

=head1 ATTRIBUTES

=head2 label

isa: Str

The human readable description of this rule

=cut

has 'label'    => (
    is      => 'rw',
    isa     => 'Str'
);

=head2 condition_type

isa: One of: C<or> or C<and>

Type of condition checking

=cut

has 'condition_type'    => (
    is      => 'rw',
    isa     => subtype('Str' => where { $_ =~ /^(?:or|and)$/}),
    default => sub { return 'and'; }
);

=head2 conditions

isa: ArrayRef of L<Zaaksysteem::Backend::Rules::Rule::Condition>

List of conditions

=cut

has 'conditions'    => (
    is      => 'rw',
    isa     => 'Maybe[ArrayRef[Zaaksysteem::Backend::Rules::Rule::Condition]]'
);

=head2 then

isa: ArrayRef of L<Zaaksysteem::Backend::Rules::Rule::Action>

List of actions to run when conditions have met.

=cut

has 'then'    => (
    is      => 'rw',
    default => sub { []; },
    isa     => 'Maybe[ArrayRef[Zaaksysteem::Backend::Rules::Rule::Action]]'
);


=head2 else

isa: ArrayRef of L<Zaaksysteem::Backend::Rules::Rule::Action>

List of actions to run when conditions have not met.

=cut

has 'else'    => (
    is      => 'rw',
    default => sub { []; },
    isa     => 'Maybe[ArrayRef[Zaaksysteem::Backend::Rules::Rule::Action]]'
);


=head2 rules_params

=cut

has 'rules_params'     => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {}; },
);

=head1 CONSTRUCTION

During construction we build the rule, by calling _build_rule on this object. You
may extend this.

=cut

sub BUILD {
    my $self        = shift;

    $self->_build_rule;
}

=head1 METHODS

=head2 validate

Arguments: [ \%PARAMS ]

    my $validation_result = $rule->validate(
        {
            'case.casetype.designation_of_confidentiality' => '-',
            'case.casetype.extension' => 'Nee',
            'case.casetype.lex_silencio_positivo' => 'Nee',
            'case.casetype.node.id' => 626,
            'case.casetype.penalty' => 'Nee',
            'case.casetype.publication' => 'Nee',
            'case.casetype.registration_bag' => 'Nee',
            'case.casetype.suspension' => 'Nee',
            'case.casetype.wkpb' => 'Nee',
            'case.casetype.objection_and_appeal' => 'Nee',
            'case.channel_of_contact' => 'email',
            'case.confidentiality' => 'public',
            'case.number_status' => 1,
            'case.payment_status' => undef,
            'case.requestor.house_number' => 42,
            'case.requestor.subject_type' => 'natuurlijk_persoon',
            'case.requestor.zipcode' => '1011PZ',
            'attribute.aantal_kentekens' => '1',
            'attribute.kenteken' => '63-SPG-4'
            'attribute.extra_kenteken1' => '',
            'attribute.extra_kenteken2' => '123-aa-bb',
        }
    );

    print "Visible: " . join(', ',  @{ $validation_result->visible_attributes });
    print "Hidden: " . join(', ',  @{ $validation_result->hidden_attributes });

    ### Prints
    # Visible: attribute.aantal_kentekens, attribute.kenteken
    # Hidden: attribute.extra_kenteken1, attribute.extra_kenteken2

Will return the validation result according to the given parameters, or returns
the last known validation profile.

=cut

has '_validate' => (
    is      => 'rw',
    isa     => 'Maybe[Zaaksysteem::Backend::Rules::ValidationResults]',
);

sub validate {
    my $self = shift;
    my ($result_object, $params, $case) = @_;

    return $self->_validate unless $params;
    return $self->_validate($self->_generate_validation($result_object, $params, $case));
}

sub _generate_validation {
    my $self          = shift;
    my $result_object = shift;
    my $params        = shift;
    my $case          = shift;

    if ($self->_conditions_match($params)) {
        for my $action (@{ $self->then }) {
            _validate_action($action, $result_object, $params, $case);
        }
    }
    else {
        for my $action (@{ $self->else }) {
            _validate_action($action, $result_object, $params, $case);
        }
    }

    return $result_object;
}

sub _validate_action {
    my ($action, $result_object, $params, $case) = @_;

    $action->_populate_validation_results($result_object, $params, $case);
    push(@{ $result_object->actions }, $action);
    return;
}

sub _conditions_match {
    my $self            = shift;
    my $params          = shift;

    my $matchfound      = 0;
    for my $condition (@{ $self->conditions }) {
        if ($condition->validate($params)) {
            return 1 if lc($self->condition_type) eq 'or';
            $matchfound = 1;
        } else {
            return undef if lc($self->condition_type) eq 'and';
            $matchfound = 0;
        }
    }

    return $matchfound ? $matchfound : undef;
}

=head2 add_condition

Arguments: \%OPTIONS

    $self->add_condition({
        attribute   => 'attribute.beer',
        values      => 'heineken'
    })

Add a condition to this rule object.

=cut

sub add_condition {
    my $self        = shift;
    my $params      = shift;

    my @conditions;
    @conditions     = @{ $self->conditions } if $self->conditions;

    my $condition   = Zaaksysteem::Backend::Rules::Rule::Condition->new(
        %$params
    );

    push(
        @conditions,
        $condition
    );

    $self->conditions(\@conditions);
}

=head2 add_action

Arguments: \%OPTIONS

    $self->add_action('then', 'set_value', {
        data    => {
            attribute   => 'attribute.kentek'
            values      => ['22-33-AB'],
        }
    })

Add a action to this rule object.

=cut

sub add_action {
    my $self        = shift;
    my $destination = lc(shift);
    my $type        = lc(shift);
    my $arguments   = shift;

    throw(
        'rules/rule/add_action/unknown_destination',
        'First argument (destination) needs to be one string: "then" or "else"'
    ) unless $destination && $destination =~ /^(then|else)$/;

    my $action_type = ACTION_TYPES->{$type};

    throw(
        'rules/rule/add_action/unknown_type',
        'Second argument (type) missing or invalid, choose one of: ' . join(',', keys %{ ACTION_TYPES() })
    ) unless $type && $action_type;

    throw(
        'rules/rule/add_action/unknown_parameters',
        'Thirs argument (arguments) needs to be a hash of parameters'
    ) unless $arguments && ref $arguments eq 'HASH';

    my $typeclass = __PACKAGE__ . "::Action::$action_type";

    my $action = try {
        return $typeclass->new(
            type => $type,
            label => $self->label,
            %$arguments
        );
    }
    catch {
        $self->log->error($_);
        return;
    };

    if ($action && $action->integrity_verified) {
        push(@{ $self->$destination }, $action);
    } else {
        $self->log->warn(
            sprintf(
                "Unable to verify rule of type %s with arguments: %s",
                $type, dump_terse($arguments)
            )
        );
        return;
    }

    return 1;
}

=head2 TO_JSON

    {
        conditions  => {
            type        => 'and',
            conditions  => [
                [...]
            ],
            then        => [
                [...]
            ],
            else        => [
                [...]
            ]
        }
    }

Turns this object in a JSON representation.

=cut


sub TO_JSON {
    my $self        = shift;

    return {
        conditions  => {
            type        => $self->condition_type,
            conditions  => $self->conditions,
        },
        else        => $self->else,
        then        => $self->then,
        label       => $self->label,
    };
}

=head2 integrity_verified

Makes sure this single rule is integrity verified. Will not allow rules
which contain no "then" actions or no conditions

=cut

sub integrity_verified {
    my $self        = shift;

    if (!@{ $self->then }) {
        return;
    }

    if (!@{ $self->conditions }) {
        return;
    }

    return 1;

}

=head1 INTERNAL METHODS

=head2 _build_rule

Method in charge of generating this object from scratch

=cut

sub _build_rule {}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ACTION_TYPES

TODO: Fix the POD

=cut

=head2 BUILD

TODO: Fix the POD

=cut

