package Zaaksysteem::Backend::BedrijfAuthenticatie::Component;
use Moose;

with 'Zaaksysteem::Roles::Password';

=head1 NAME

Zaaksysteem::Backend::BedrijfAuthenticatie::Component - A DBIx component

=cut

BEGIN { extends 'DBIx::Class' }

around check_password => sub {
    my $orig = shift;
    my $self = shift;
    my $password = shift;

    if ($self->password !~ /^\{.+\}/ && $self->password eq $password) {
        $self->update_password($password);
        return 1;
    }

    return $orig->($self, $password);
};


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

