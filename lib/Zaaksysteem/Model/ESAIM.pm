package Zaaksysteem::Model::ESAIM;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::Search::Elasticsearch::MappingModel',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::ESAIM - Catalyst model factory for
L<Zaaksysteem::Search::Elasticsearch::MappingModel>.

=head1 DESCRIPTION

This model factory produces instances of
L<Zaaksysteem::Search::Elasticsearch::MappingModel>.

The brevity of the model name C<ESAIM> was chosen for developer convenience.

=head1 METHODS

=head2 prepare_arguments

Implements logic for L<Catalyst::Model::Factory::PerRequest>.

Aggregates configuration for the constructor of
L<Zaaksysteem::Search::Elasticsearch::MappingModel>.

=cut

sub prepare_arguments {
    my ($self, $c, @args) = @_;

    return {
        elasticsearch => $c->model('Elasticsearch', { cluster => 'Object' }),
        index_name => $c->config->{ instance_hostname }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
