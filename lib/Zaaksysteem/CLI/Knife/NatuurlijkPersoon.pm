package Zaaksysteem::CLI::Knife::NatuurlijkPersoon;
use Moose::Role;
use Zaaksysteem::CLI::Knife::Action;
use BTTW::Tools;

my $knife = 'subject';

=head1 NAME

Zaaksysteem::CLI::Knife::NatuurlijkPersoon - NatuurlijkPersoon CLI actions

=cut

register_knife $knife => (
    description => "Subject related functions"
);

register_category natuurlijk_persoon => (
    knife       => $knife,
    description => "Natuurlijk persoon"
);

register_action list => (
    knife       => $knife,
    category    => 'natuurlijk_persoon',
    description => 'List natuurlijk personen',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $rs = $self->schema->resultset('NatuurlijkPersoon')->search_rs(
            { deleted_on => undef },
            { order_by => 'burgerservicenummer' }
        );
        while (my $np = $rs->next) {
            $self->log->info($np->bsn);
        }
    }
);

register_action info => (
    knife       => $knife,
    category    => 'natuurlijk_persoon',
    description => 'Get information about a NP',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $params = $self->assert_knife_params(
            bsn => 'Str'
        );

        my $np = $self->_get_by_bsn($params->{bsn});
        $self->log->info($np->bsn);
    }
);


register_action delete => (
    knife       => $knife,
    category    => 'natuurlijk_persoon',
    description => 'Search controlpanels',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $params = $self->assert_knife_params(
            bsn => 'Str'
        );

        $self->delete_np($self->_get_by_bsn($params->{bsn}));
    }
);

=head2 delete_np

Delete a natuurlijk persoon from Zaaksysteem. This includes the logic that an objectsubscription will be deleted as well.

=cut

sig delete_np => 'Zaaksysteem::Model::DB::NatuurlijkPersoon';

sub delete_np {
    my ($self, $np) = @_;

    my $now = DateTime->now();
    $self->log->info(sprintf("Deleting natuurlijk persoon with BSN %s [%d]", $np->bsn, $np->id));
    if ($np->authenticated) {
        # There should only be one, but just in case.. delete everything in
        # sight. We shouldn't die when deleting with multiple object
        # subscriptions, it is wrong anyway
        my $rs = $self->schema->resultset('ObjectSubscription')->search_rs({
            local_id     => $np->id,
            local_table  => 'NatuurlijkPersoon',
            date_deleted => undef,
        });
        if ($rs->first) {
            $self->log->info("Deleting object subscriptions " . join(", ", $rs->get_column('id')->all));
            $rs->update({date_deleted => $now});
        }
    }
    $np->update({deleted_on => $now});
}

sig _get_by_bsn => 'Num';

sub _get_by_bsn {
    my ($self, $bsn) = @_;

    return $self->schema->resultset('NatuurlijkPersoon')->get_by_bsn($bsn);
}

1;

=head1 SEE ALSO

L<Zaaksysteem::CLI::Knife>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
