package Zaaksysteem::DB::Component::Logging::Case::Thread::Link;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf('Bericht toegevoegd aan zaak %s',
        $self->data->{ case_id },
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

