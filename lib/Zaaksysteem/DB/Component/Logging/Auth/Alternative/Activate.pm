package Zaaksysteem::DB::Component::Logging::Auth::Alternative::Activate;
use Moose::Role;

has subject => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        $self->rs('Subject')->find($self->component_id);
    }
);

=head2 onderwerp

Pretty print the subject

=cut

sub onderwerp {
    my $self = shift;

    return sprintf("Alternatieve authenticatie account '%s' is %s",
        $self->data->{username},
        $self->data->{action} eq 'disable' ? "gedeactiveerd" : "geactiveerd");
}

sub event_category { 'system'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
