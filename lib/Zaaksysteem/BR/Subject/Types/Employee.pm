package Zaaksysteem::BR::Subject::Types::Employee;

use Moose::Role;
use BTTW::Tools;

use Zaaksysteem::BR::Subject::Constants qw/SUBJECT_CONFIGURATION SUBJECT_MAPPING/;

use constant SUBJECT_TYPE => 'employee';
use constant SCHEMA_TABLE => 'Subject';

=head1 NAME

Zaaksysteem::BR::Subject::Types::Employee - Bridge specific role for this type

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating various types.

=head1 ATTRIBUTES

=head2 _table_id

Private method: contains the reference to the original table ('natuurlijk_persoon.id')

=cut

has '_table_id' => (
    is      => 'rw',
    isa     => 'Int',
);


=head1 METHODS

=head2 new_from_row

=cut

sub new_from_row {
    my ($class, $row) = @_;

    my $mapping     = SUBJECT_CONFIGURATION->{ SUBJECT_TYPE() }->{mapping};

    my %values      = map(
        { $mapping->{$_} => $row->$_ }
        grep({ defined $row->$_ } keys %$mapping)
    );

    my $subject     = $class->new(%values);

    $subject->id($row->uuid);
    $subject->_table_id($row->id);


    if ($row->properties->{ email_address }) {
        $subject->email_address($row->properties->{ email_address });
    }
    elsif ($row->properties->{ mail }) {
        $subject->email_address($row->properties->{ mail });
    }

    if ($row->properties->{ phone_number }) {
        $subject->phone_number($row->properties->{ phone_number });
    }

    if ($row->settings) {
        $subject->settings($row->settings);
    }

    my $positions = $row->position_objects;

    $subject->positions($positions) if scalar @{ $positions };

    return $subject;
}

=head2 new_from_params

=cut

sub new_from_params {
    my $class       = shift;
    my $params      = { %{ (shift || {}) } };

    return $class->new(%$params);
}


=head2 _build_display_name

=cut

sub _build_display_name {
    my $self            = shift;

    return $self->display_name;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
