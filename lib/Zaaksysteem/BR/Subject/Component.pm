package Zaaksysteem::BR::Subject::Component;

use Moose::Role;

use Zaaksysteem::Object::Types::Subject;

with qw/Zaaksysteem::BR::Subject::Utils/;


=head1 NAME

Zaaksysteem::BR::Subject::Component - A generic component for subject in zs.nl

=head1 DESCRIPTION

Add this generic component to all the "subject" tables. Like "Bedrijf", "NatuurlijkPersoon" and
"Subject".

=head1 METHODS

=head2 as_object

    my $object = $np->as_object;

Inflates this component to an object suitable for our object system

=cut

sub as_object {
    my $self        = shift;

    return Zaaksysteem::Object::Types::Subject->new_from_row($self, @_);
}

=head2 cached_subscriptions

Returns: ARRAYREF of Zaaksysteem::Schema::ObjectSubscription

Return an arrayref of subscriptions linked to this NP

=cut

has 'cached_subscriptions' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    clearer => '_clear_cached_subscriptions',
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
